package com.coherentsolutions.automation.tst.pom.yandex;

import com.codeborne.selenide.SelenideElement;
import com.coherentsolutions.automation.yaf.web.pom.WebPage;
import lombok.Data;
import org.springframework.stereotype.Component;

import static com.codeborne.selenide.Selenide.$;

@Component
@Data
public class YandexSearchSelenidePage extends WebPage {

    SelenideElement searchInput = $(".input__control");

    SelenideElement searchButton = $(".button_theme_websearch");

    public void search(String text) {
        searchInput.setValue(text);
        searchButton.click();
    }
}
