package com.coherentsolutions.automation.tst.domain;

import com.coherentsolutions.automation.yaf.bean.factory.data.YafData;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@YafData
public class TestData {

    List<String> searchStrings;
}
