package com.coherentsolutions.automation.tst.pom.adaptive;

import com.coherentsolutions.automation.yaf.web.pom.WebPage;
import org.springframework.stereotype.Component;

@Component
public abstract class AdaptiveSamplePage extends WebPage {

    //https://html5up.net/uploads/demos/editorial/

    public abstract void openGenericPage();
}
