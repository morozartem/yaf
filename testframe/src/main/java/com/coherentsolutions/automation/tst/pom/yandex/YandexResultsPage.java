package com.coherentsolutions.automation.tst.pom.yandex;

import com.coherentsolutions.automation.yaf.wait.driver.WaitConsts;
import com.coherentsolutions.automation.yaf.wait.driver.WaitFor;
import com.coherentsolutions.automation.yaf.wait.driver.waits.visibility.VisibilityWait;
import com.coherentsolutions.automation.yaf.wait.driver.waits.visibility.WaitForVisible;
import com.coherentsolutions.automation.yaf.web.pom.WebPage;
import lombok.Data;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Data
public class YandexResultsPage extends WebPage {

    public String name;

    @FindBy(css = ".serp-item")
    @WaitFor()
    List<WebElement> results;// = driverHolder.getDriver().findElements("ddddd");


    //By b = new YafBy().add(Android, By.cssSelector("sdfsdfs")).add(IOS, "dfsdfsdg");

    @WaitForVisible()
    @FindBy(css = ".serp-adv__found")
    WebElement resultsCount;


    public void getTop5Resuext(int x) {
//        int i = x+1;
//        By s = "sfa"df++ds+sd+gs+dgs;
//        WebElement e = driverHolder.getDriver().findElement(by);
        resultsCount = new RemoteWebElement();
        resultsCount.click();

        waitService.waitFor(new VisibilityWait(resultsCount).withWaitConsts(WaitConsts.SUPER_FAST));
        //waitService.waitForVisible(resultsCount); //todo impl

        resultsCount.click();
    }

    public List<String> getTop5ResultsText() {
        return results.stream().map(e -> e.getText()).limit(5).collect(Collectors.toList());
    }
}
