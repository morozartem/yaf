package com.coherentsolutions.automation.tst.pom.adaptive;

import com.codeborne.selenide.SelenideElement;
import com.coherentsolutions.automation.yaf.condition.YafCondition;
import org.springframework.stereotype.Component;

import static com.codeborne.selenide.Selenide.$x;

@Component
@YafCondition(width = 500)
public class MobileAdaptiveSamplePage extends AdaptiveSamplePage {

    SelenideElement genericMenu = $x("//nav[@id='menu']/ul/li[2]");

    SelenideElement burgerMenuButton = $x("//a[@href='#sidebar']");

    @Override
    public void openGenericPage() {
        burgerMenuButton.click();
        genericMenu.click();
    }
}
