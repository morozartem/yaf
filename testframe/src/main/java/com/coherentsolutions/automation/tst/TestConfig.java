package com.coherentsolutions.automation.tst;

import com.coherentsolutions.automation.yaf.config.env.Env;
import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.config.yaf.YafConfig;
import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.enums.Browser;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@SpringBootConfiguration
@EnableAutoConfiguration
@Slf4j
public class TestConfig extends YafConfig {

    public TestConfig(ApplicationArguments arguments) {
        super(arguments);
        log.info("Loading {} config", this.getClass().getName());
    }

//    @Bean
//    public YafBeanFieldInterceptor interceptor(){
//        return Aspects.aspectOf(YafBeanFieldInterceptor.class);
//    }

    @Bean
    //@Profile("dev")
    @Scope(SCOPE_PROTOTYPE)
    public Env stub() {
        Env env = new Env();
        EnvItem envItem = new EnvItem();
        envItem.setBrowser(Browser.CHROME);
        envItem.setType(DeviceType.WEB);
        envItem.setName(Consts.DEFAULT);
        //envItem.getCapabilities().put(Consts.CAP_RESOLUTION, "400x1000");
        envItem.setHeadless(true);
        env.add(envItem);
        return env;
    }

}
