package com.coherentsolutions.automation.tst.pom.adaptive;

import com.codeborne.selenide.SelenideElement;
import com.coherentsolutions.automation.yaf.condition.YafCondition;
import org.springframework.stereotype.Component;

import static com.codeborne.selenide.Selenide.$x;

@Component
@YafCondition
public class DesktopAdaptiveSamplePage extends AdaptiveSamplePage {

    SelenideElement genericMenuItem = $x("//nav[@id='menu']/ul/li[2]");

    @Override
    public void openGenericPage() {
        genericMenuItem.click();
    }
}
