package com.coherentsolutions.automation.tst;

import com.coherentsolutions.automation.tst.domain.TestData;
import com.coherentsolutions.automation.yaf.test.YafTest;
import com.coherentsolutions.automation.yaf.testng.YafTestNgTest;
import com.coherentsolutions.automation.yaf.testng.dataprovider.annotations.LangSource;
import com.coherentsolutions.automation.yaf.testng.dataprovider.annotations.RandomStringSource;
import com.coherentsolutions.automation.yaf.utils.i18n.I18nService;
import com.coherentsolutions.automation.yaf.utils.templates.TemplateValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.Locale;

import static com.coherentsolutions.automation.yaf.utils.i18n.Lang.EN;
import static com.coherentsolutions.automation.yaf.utils.i18n.Lang.RU;


public class DataTests extends YafTestNgTest {


    TestData testData;

    @Autowired
    I18nService i18nService;

    //getting data from suite file
    @Test
    @Parameters({"searchString"})
    public void dataFromXmlTest(String searchString) {
        System.out.println(searchString);
    }

    @Test
    @RandomStringSource(size = 10)
    public void yafDataProviderTest(String searchString) {
        System.out.println(searchString);
    }

    @Test
    public void yafDataTest() {
        System.out.println(testData.getSearchStrings());
    }

    @Test
    @YafTest(dataFile = "data")
    @TemplateValue(key = "user", value = "asd")
    @TemplateValue(key = "user2", value = "asd2222")
    @LangSource({RU, EN})
    public void yafDataTestByDataId(Locale locale) {
        System.out.println(testData.getSearchStrings());
    }

    @Test
    public void localizationTest() {
        System.out.println(i18nService.getMessage("hello"));
    }


}
