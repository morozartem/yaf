package com.coherentsolutions.automation.tst;

import com.coherentsolutions.automation.tst.pom.yandex.YandexResultsPage;
import com.coherentsolutions.automation.tst.pom.yandex.YandexSearchSelenidePage;
import com.coherentsolutions.automation.yaf.enums.Severity;
import com.coherentsolutions.automation.yaf.test.YafTest;
import com.coherentsolutions.automation.yaf.testng.YafTestNgTest;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.assertj.core.api.Assertions.assertThat;


public class YandexSelenideTest extends YafTestNgTest {

    YandexSearchSelenidePage yandexSearchSelenidePage;

    YandexResultsPage yandexResultsPage;

    @Test
    @YafTest(name = "YandexSearchTest", tmsIds = {"TMS-12"}, severity = Severity.CRITICAL)
    public void testYandexSearchPlain() {
        open("http://ya.ru");
        String searchString = faker.animal().name();
        yandexSearchSelenidePage.search(searchString);
        //temp solution! will be extra 'module' for waits
        WebDriverWait wait = new WebDriverWait(testExecutionContext.getWebDriverHolder().getDriver(), 5);
        wait.until(ExpectedConditions.urlContains("yandex"));

        assertThat(yandexResultsPage.getResults()).hasSizeGreaterThan(5).as("Number of results are less than 5!");
    }

    @Test
    @YafTest(name = "YandexSearchTest", tmsIds = {"TMS-12"}, severity = Severity.CRITICAL)
    public void testYandexSearchPlain2() {
        open("http://ya.ru");
      //  yandexResultsPage.getResults().get(0).isDisplayed();
        yandexResultsPage.getResultsCount().isDisplayed();
        assertThat(1).isEqualTo(1);
    }
}
