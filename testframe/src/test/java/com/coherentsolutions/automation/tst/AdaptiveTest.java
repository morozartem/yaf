package com.coherentsolutions.automation.tst;


import com.coherentsolutions.automation.tst.pom.adaptive.AdaptiveSamplePage;
import com.coherentsolutions.automation.yaf.test.YafTest;
import com.coherentsolutions.automation.yaf.testng.YafTestNgTest;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.assertj.core.api.Assertions.assertThat;

public class AdaptiveTest extends YafTestNgTest {

   // AdaptiveSamplePage adaptiveSamplePage;

    @Test
    @YafTest
    public void adaptiveTest() {
        open("https://html5up.net/uploads/demos/editorial/");
        AdaptiveSamplePage adaptiveSamplePage = getObject(AdaptiveSamplePage.class);
        System.out.println("Getting page " + adaptiveSamplePage.getClass().getCanonicalName());
        adaptiveSamplePage.openGenericPage();

        WebDriver driver = testExecutionContext.getWebDriverHolder().getDriver();

        assertThat(driver.getTitle()).contains("Generic");
    }
}
