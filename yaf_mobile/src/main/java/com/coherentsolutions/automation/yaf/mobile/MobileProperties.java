package com.coherentsolutions.automation.yaf.mobile;

import com.coherentsolutions.automation.yaf.config.yaf.ModuleProperties;
import com.coherentsolutions.automation.yaf.consts.Consts;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = Consts.FRAMEWORK_NAME + ".mobile")
public class MobileProperties extends ModuleProperties {

    AppiumMobileProperties android;
    AppiumMobileProperties ios;

    @Data
    public static class AppiumMobileProperties {

        AppiumServiceProperties service;
        Map<String, String> constsCapabilities;
    }

    @Data
    public static class AppiumServiceProperties {

        String ip;
        int port;

    }

}
