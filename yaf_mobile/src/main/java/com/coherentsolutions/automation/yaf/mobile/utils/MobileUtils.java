package com.coherentsolutions.automation.yaf.mobile.utils;

import com.coherentsolutions.automation.yaf.bean.pom.IContextual;
import com.coherentsolutions.automation.yaf.bean.utils.Utils;
import io.appium.java_client.AppiumDriver;


public abstract class MobileUtils extends Utils implements IContextual {

    private AppiumDriver driver;

    protected AppiumDriver getDriver() {
        if (driver == null) {
            driver = (AppiumDriver) testExecutionContext.getMobileDriverHolder().getDriver();
        }
        return driver;
    }
}
