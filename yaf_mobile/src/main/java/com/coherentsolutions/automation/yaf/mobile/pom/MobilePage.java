package com.coherentsolutions.automation.yaf.mobile.pom;

import com.coherentsolutions.automation.yaf.bean.pom.Page;
import com.coherentsolutions.automation.yaf.drivers.properties.DriverProperties;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class MobilePage extends Page {

    @Autowired
    DriverProperties driverProperties;

    @Override
    protected void init() {
        //todo think about getting names from annotations or someWhereElse
        driverHolder = testExecutionContext.getMobileDriverHolder();

        if (driverProperties.getMobile().isUsePageFactory()) {
            PageFactory.initElements(driverHolder.getDriver(), this);
        }
    }
}
