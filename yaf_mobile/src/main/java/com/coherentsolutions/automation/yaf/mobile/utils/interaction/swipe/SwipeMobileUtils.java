package com.coherentsolutions.automation.yaf.mobile.utils.interaction.swipe;

import com.coherentsolutions.automation.yaf.mobile.utils.MobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.scroll.ScrollMobileUtils;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SwipeMobileUtils extends MobileUtils {

    @Autowired
    ScrollMobileUtils scrollMobileUtils;

    public void swipeToScreensTop() {
        int numberOfIterations = 5;
        for (int i = 0; i < numberOfIterations; i++) {
            singleSwipeDown();
        }
    }

    public void singleSwipe(MobileElement scrollableElement, double coeffFrom, double coeffTo) {
        singleSwipeFromPoint1ToPoint2(getSwipePoint(scrollableElement, coeffFrom), getSwipePoint(scrollableElement, coeffTo));
    }

    public void singleSwipeDown() {
        singleSwipeFromPoint1ToPoint2(getTopSwipePoint(), getBottomSwipePoint());
    }

    private void singleSwipeFromPoint1ToPoint2(PointOption point1, PointOption point2) {
        if (point1 != null && point2 != null) {
            new TouchAction(getDriver()).longPress(point1).moveTo(point2).release().perform();
        }
    }

    private PointOption getTopSwipePoint() {
        return getSwipePoint(0.2);
    }

    private PointOption getBottomSwipePoint() {
        return getSwipePoint(0.8);
    }

    private PointOption getSwipePoint(MobileElement element, double coeff) {
        Pair<Point, Dimension> params = scrollMobileUtils.getScrollableAreaLocationAndSize(element);
        return PointOption.point(getX(params), getY(params, coeff));
    }

    private PointOption getSwipePoint(double coeff) {
        Pair<Point, Dimension> params;
        if ((params = scrollMobileUtils.getScrollableAreaLocationAndSize()) != null) {
            return PointOption.point(getX(params), getY(params, coeff));
        } else {
            return null;
        }
    }

    private int getX(Pair<Point, Dimension> params) {
        return params.getRight().getWidth() / 2 + params.getLeft().getX();
    }

    private int getY(Pair<Point, Dimension> params, double coeff) {
        return params.getLeft().getY() + (int) (params.getRight().getHeight() * coeff);
    }


    public void singleSwipeUp() {
        singleSwipeFromPoint1ToPoint2(getBottomSwipePoint(), getTopSwipePoint());
    }


}
