package com.coherentsolutions.automation.yaf.mobile.utils.interaction.scroll;

import com.coherentsolutions.automation.yaf.mobile.utils.MobileUtils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public abstract class ScrollMobileUtils extends MobileUtils {

    public abstract void scrollDown();

    public abstract void scrollUp();

    public Pair<Point, Dimension> getScrollableAreaLocationAndSize(MobileElement element) {
        return Pair.of(element.getLocation(), element.getSize());
    }

    public abstract Pair<Point, Dimension> getScrollableAreaLocationAndSize();

    protected Pair<Point, Dimension> getScrollableAreaLocationAndSize(By scrollView, By view) {
        List<MobileElement> scrollViewElements;
        AppiumDriver appiumDriver = getDriver();
        List<MobileElement> scrollViewElementsAndroid1 = appiumDriver.findElements(scrollView);
        List<MobileElement> scrollViewElementsAndroid2 = appiumDriver.findElements(view);
        if ((scrollViewElements = scrollViewElementsAndroid1).size() > 0 || (scrollViewElements = scrollViewElementsAndroid2).size() > 0) {
            return Pair.of(scrollViewElements.get(0).getLocation(), scrollViewElements.get(0).getSize());
        } else {
            //return Pair.of(new Point(0, 0), driverManager.getMobileDriver().manage().window().getSize());
            return null;
        }
    }
}
