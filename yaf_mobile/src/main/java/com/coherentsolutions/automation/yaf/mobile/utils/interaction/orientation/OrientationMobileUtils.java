package com.coherentsolutions.automation.yaf.mobile.utils.interaction.orientation;

import com.coherentsolutions.automation.yaf.mobile.utils.MobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.swipe.SwipeMobileUtils;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebDriverException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import static java.lang.String.format;
import static org.openqa.selenium.ScreenOrientation.LANDSCAPE;
import static org.openqa.selenium.ScreenOrientation.PORTRAIT;

@Component
@Slf4j
public class OrientationMobileUtils extends MobileUtils {

    @Autowired
    SwipeMobileUtils swipeMobileUtils;

    public boolean isInPortraitMode() {
        return getDriver().getOrientation().equals(PORTRAIT);
    }

    public boolean isInLandscapeMode() {
        return getDriver().getOrientation().equals(LANDSCAPE);
    }

    public void setOrientation(ScreenOrientation orientation) {
        if (!getActualOrientation().equals(orientation)) {
            rotateToOrientationAndAssert(orientation);
        }
    }

    public void rotateToLandscape() {
        rotate(LANDSCAPE);
    }

    public void assertLandscapeOrientation() {
        assertOrientation(LANDSCAPE);
    }

    public void rotateToLandscapeAndAssert() {
        rotateToOrientationAndAssert(LANDSCAPE);
    }

    public void rotateToLandscapeAndAssertAppRefusedToRotate() {
        rotateToOrientationAndAssertAppRefusedToRotate(LANDSCAPE, PORTRAIT);
    }

    public void rotateToPortrait() {
        rotate(PORTRAIT);
    }

    public void assertPortraitOrientation() {
        assertOrientation(PORTRAIT);
    }

    public void rotateToPortraitAndAssert() {
        rotateToOrientationAndAssert(PORTRAIT);
    }

    public void rotateToPortraitAndAssertAppRefusedToRotate() {
        rotateToOrientationAndAssertAppRefusedToRotate(PORTRAIT, LANDSCAPE);
    }

    private void rotateToOrientationAndAssert(ScreenOrientation orientation) {
        rotate(orientation);
        assertOrientation(orientation);
    }

    private void rotateToOrientationAndAssertAppRefusedToRotate(ScreenOrientation expectedOrientation, ScreenOrientation actualOrientation) {
        try {
            rotate(expectedOrientation);
        } catch (WebDriverException e) {
            //todo???
        }
        assertOrientation(actualOrientation);
    }

    private void rotate(ScreenOrientation orientation) {
        log.info("Rotating screen to [{}] orientation", orientation);
        getDriver().rotate(orientation);
        swipeMobileUtils.singleSwipeDown();
    }

    private void assertOrientation(ScreenOrientation orientation) {
        log.info("Asserting screen is in [{}] orientation", orientation);
        Assert.isTrue(getActualOrientation().equals(orientation),
                format("Screen's orientation is not a %s one", orientation)
        );
    }

    public ScreenOrientation getActualOrientation() {
        ScreenOrientation so = getDriver().getOrientation();
        log.info("Getting actual screen's orientation: {}", so);
        return so;
    }


}
