package com.coherentsolutions.automation.yaf.mobile.utils.interaction.scroll;

import com.coherentsolutions.automation.yaf.mobile.condition.Android;
import io.appium.java_client.MobileBy;
import io.appium.java_client.ios.IOSDriver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
@Slf4j
@Android
public class IOSScrollUtils extends ScrollMobileUtils {

    public IOSDriver getDriver() {
        return (IOSDriver) super.getDriver();
    }

    @Override
    public void scrollDown() {
        scroll("down");
    }

    @Override
    public void scrollUp() {
        scroll("up");
    }

    protected void scroll(String direction) {
        HashMap<String, String> obj = new HashMap<>();
        obj.put("direction", direction);
        getDriver().executeScript("mobile: scroll", obj);
    }

    @Override
    public Pair<Point, Dimension> getScrollableAreaLocationAndSize() {
        return getScrollableAreaLocationAndSize(
                MobileBy.iOSClassChain("**/XCUIElementTypeScrollView"),
                MobileBy.iOSClassChain("**/XCUIElementTypeTable")
        );
    }
}
