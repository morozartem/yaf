package com.coherentsolutions.automation.yaf.mobile.driver.holder;

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AppiumDriverHolder extends DriverHolder {

    @Getter
    @Setter
    AppiumDriverLocalService appiumService;

    public AppiumDriverHolder(EnvItem envItem) {
        super(envItem);

    }

    @Override
    public void quit() {
        try {
            appiumService.stop();
            getDriver().quit();
        } catch (Exception ex) {
            log.error("xxxx");
        }
    }
}
