package com.coherentsolutions.automation.yaf.mobile.utils.interaction.scroll;

import com.coherentsolutions.automation.yaf.mobile.condition.Android;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.springframework.stereotype.Component;

import static io.appium.java_client.touch.offset.PointOption.point;

@Component
@Slf4j
@Android
public class AndroidScrollUtils extends ScrollMobileUtils {

    public AndroidDriver getDriver() {
        return (AndroidDriver) super.getDriver();
    }

    @Override
    public void scrollDown() {
        scroll(.75, .2);
    }

    @Override
    public void scrollUp() {
        scroll(.15, .8);
    }

    protected void scroll(double heightCoef, double moveToCoef) {
        AppiumDriver appiumDriver = getDriver();
        Dimension dimension = appiumDriver.manage().window().getSize();
        int visibleBottomPointHeight = (int) (dimension.getHeight() * heightCoef);
        int middleWidthPoint = dimension.getWidth() / 2;
        int moveToY = (int) (dimension.getHeight() * moveToCoef);
        new TouchAction(appiumDriver)
                .press(point(middleWidthPoint, visibleBottomPointHeight)).waitAction()
                .moveTo(point(middleWidthPoint, moveToY)).release().perform();
    }

    @Override
    public Pair<Point, Dimension> getScrollableAreaLocationAndSize() {
        return getScrollableAreaLocationAndSize(
                By.xpath("//android.widget.ScrollView"), By.xpath("//android.widget.ExpandableListView")
        );
    }
}
