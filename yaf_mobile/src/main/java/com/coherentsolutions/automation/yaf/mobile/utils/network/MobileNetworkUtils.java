package com.coherentsolutions.automation.yaf.mobile.utils.network;

import com.coherentsolutions.automation.yaf.mobile.utils.MobileUtils;

public abstract class MobileNetworkUtils extends MobileUtils {

    public abstract void switchBluetoothOn();

    public abstract void switchBluetoothOff();

    public abstract void pairBluetooth(String device);

    public abstract void unPairBluetooth(String device);

    //be aware that it is inner methods and it does not prepare app state
    protected abstract int getBluetoothState();

    protected abstract boolean isPairedBluetooth(String device);
}
