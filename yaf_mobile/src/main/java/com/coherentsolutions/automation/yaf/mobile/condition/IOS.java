package com.coherentsolutions.automation.yaf.mobile.condition;

import com.coherentsolutions.automation.yaf.condition.YafCondition;
import com.coherentsolutions.automation.yaf.enums.MobileOS;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
@Documented
@YafCondition(mobileOs = MobileOS.IOS)
public @interface IOS {
}
