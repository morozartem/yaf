package com.coherentsolutions.automation.yaf.mobile.utils.network;

import com.coherentsolutions.automation.yaf.mobile.condition.Android;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Android
public class AndroidNetworkUtils extends MobileNetworkUtils {

    @Override
    public void switchBluetoothOn() {

    }

    @Override
    public void switchBluetoothOff() {

    }

    @Override
    public void pairBluetooth(String device) {

    }

    @Override
    public void unPairBluetooth(String device) {

    }

    @Override
    protected int getBluetoothState() {
        return 0;
    }

    @Override
    protected boolean isPairedBluetooth(String device) {
        return false;
    }
}
