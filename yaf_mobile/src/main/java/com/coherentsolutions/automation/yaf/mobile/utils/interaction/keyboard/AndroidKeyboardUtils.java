package com.coherentsolutions.automation.yaf.mobile.utils.interaction.keyboard;

import com.coherentsolutions.automation.yaf.mobile.condition.Android;
import io.appium.java_client.android.AndroidDriver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Android
public class AndroidKeyboardUtils extends KeyboardMobileUtils {

    public AndroidDriver getDriver() {
        return (AndroidDriver) super.getDriver();
    }

    @Override
    public void hideKeyboard() {
        if (getDriver().isKeyboardShown()) {
            log.info("Hiding ANDROID keyboard");
            getDriver().hideKeyboard();
        }
    }

    @Override
    public void tapPhoneBackButton() {
        hideKeyboard();
        getDriver().navigate().back();
    }
}
