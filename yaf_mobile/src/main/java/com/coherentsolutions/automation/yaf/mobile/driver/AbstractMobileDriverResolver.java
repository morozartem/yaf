package com.coherentsolutions.automation.yaf.mobile.driver;

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.drivers.manager.DriverResolver;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.mobile.MobileProperties;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.Map;

import static com.coherentsolutions.automation.yaf.enums.DeviceType.MOBILE;
import static io.appium.java_client.remote.AndroidMobileCapabilityType.AVD;
import static io.appium.java_client.remote.MobileCapabilityType.AUTOMATION_NAME;
import static io.appium.java_client.remote.MobileCapabilityType.UDID;

@Component
@Slf4j
public abstract class AbstractMobileDriverResolver extends DriverResolver {


    @Override
    public DeviceType getResolverType() {
        return MOBILE;
    }

    //TODO add custom caps and device uids and proprs after prepareEnv


    protected MutableCapabilities getGeneralMobileCapabilities(EnvItem envItem, MobileProperties.AppiumMobileProperties properties, Map<String, String> customCapabilities) {
        MutableCapabilities capabilities = (MutableCapabilities) buildCapabilitiesFromEnv(envItem);

        properties.getConstsCapabilities().entrySet().forEach(e -> capabilities.setCapability(e.getKey(), e.getValue()));

        customCapabilities.entrySet().forEach(e -> capabilities.setCapability(e.getKey(), e.getValue()));

        String udid = envItem.getCapabilities().get(MobileCapabilityType.UDID);

        if (envItem.isSimulator()) {
            capabilities.setCapability(AVD, udid);
        } else {
            capabilities.setCapability(UDID, udid);
        }
        return capabilities;
    }

    protected AndroidDriver buildAndroidDriver(EnvItem envItem, URL serviceUrl, MobileProperties.AppiumMobileProperties properties, Map<String, String> customCapabilities) {
        MutableCapabilities capabilities = getGeneralMobileCapabilities(envItem, properties, customCapabilities);
        capabilities.setCapability(AUTOMATION_NAME, "UiAutomator2");
        capabilities.setCapability("allowInvisibleElements", true);
        return new AndroidDriver<>(serviceUrl, new DesiredCapabilities(capabilities));
    }

    protected IOSDriver buildIOSDriver(EnvItem envItem, URL serviceUrl, MobileProperties.AppiumMobileProperties properties, Map<String, String> customCapabilities) {
        MutableCapabilities capabilities = getGeneralMobileCapabilities(envItem, properties, customCapabilities);
        capabilities.setCapability(AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("platformName", "iOS");
        return new IOSDriver<>(serviceUrl, capabilities);
    }

    protected AppiumDriverLocalService startAppiumService(MobileProperties.AppiumMobileProperties properties) {
        AppiumServiceBuilder builder = new AppiumServiceBuilder();


        MobileProperties.AppiumServiceProperties appiumServiceProperties = properties.getService();

        if (appiumServiceProperties.getIp() != null) {
            builder = builder.withIPAddress(appiumServiceProperties.getIp());
        }
        if (appiumServiceProperties.getPort() != 0) {
            builder.usingPort(appiumServiceProperties.getPort());
        }

        AppiumDriverLocalService service = builder.build();

        service.start();
        log.info("Appium Service started with URL {} ", service.getUrl());

        return service;
    }
}
