package com.coherentsolutions.automation.yaf.mobile.utils.interaction;

import com.coherentsolutions.automation.yaf.mobile.utils.MobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.keyboard.KeyboardMobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.orientation.OrientationMobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.scroll.ScrollMobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.swipe.SwipeMobileUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


//TODO validate and refactor

@Component
@Slf4j
@Data
public class MobileInteractionUtils extends MobileUtils {


    @Autowired
    KeyboardMobileUtils keyboard;

    @Autowired
    OrientationMobileUtils orientation;

    @Autowired
    ScrollMobileUtils scroll;

    @Autowired
    SwipeMobileUtils swipe;


    //TODO think about this methods

    public void closeApp() {
        log.info("Closing app");
        getDriver().closeApp();
    }

    public void launchApp() {
        log.info("Launching app");
        getDriver().launchApp();
    }
}
