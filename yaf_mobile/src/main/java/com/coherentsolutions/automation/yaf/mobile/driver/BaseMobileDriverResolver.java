package com.coherentsolutions.automation.yaf.mobile.driver;

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.enums.MobileOS;
import com.coherentsolutions.automation.yaf.exception.DriverYafException;
import com.coherentsolutions.automation.yaf.mobile.MobileProperties;
import com.coherentsolutions.automation.yaf.mobile.driver.holder.AppiumDriverHolder;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Order()//set this resolver at last position, cause other user defined should be prior that
@Slf4j
public class BaseMobileDriverResolver extends AbstractMobileDriverResolver {


    @Autowired
    MobileProperties mobileProperties;

    @Override
    public boolean canResolve(EnvItem envItem) {
        return super.canResolve(envItem) && emptyFarm(envItem);
    }

    @Override
    public DriverHolder initDriver(EnvItem envItem) {

        WebDriver driver = null;
        log.debug("Building mobile {} driver for env {}", envItem.getMobileOS(), envItem.getName());

        AppiumDriverHolder appiumDriverHolder = new AppiumDriverHolder(envItem);

        MobileProperties.AppiumMobileProperties props = MobileOS.ANDROID.equals(envItem.getMobileOS()) ? mobileProperties.getAndroid() : mobileProperties.getIos();
        AppiumDriverLocalService service = startAppiumService(props);

        Map<String, String> customCapabilities = new HashMap<>();

        try {

            switch (envItem.getMobileOS()) {
                case ANDROID: {

                    driver = buildAndroidDriver(envItem, service.getUrl(), props, customCapabilities);
                    break;
                }
                case IOS: {

                    driver = buildIOSDriver(envItem, service.getUrl(), props, customCapabilities);
                    break;
                }
                //todo add others
                default: {
                    throw new DriverYafException("SSSSSS");
                }
            }

            log.debug("****** CREATE NEW MOBILE DRIVER ******** ");

            appiumDriverHolder.setDeviceType(DeviceType.MOBILE).setDriver(driver);
            appiumDriverHolder.setAppiumService(service);
        } finally {
            if (appiumDriverHolder.getDriver() == null) {
                log.debug("Lets stop service, cause we could not create mobile driver!");
                service.stop();
            }
        }
        return appiumDriverHolder;
    }


}
