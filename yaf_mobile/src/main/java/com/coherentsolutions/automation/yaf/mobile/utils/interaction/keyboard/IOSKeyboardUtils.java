package com.coherentsolutions.automation.yaf.mobile.utils.interaction.keyboard;

import com.coherentsolutions.automation.yaf.mobile.condition.IOS;
import io.appium.java_client.MobileBy;
import io.appium.java_client.ios.IOSDriver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@IOS
public class IOSKeyboardUtils extends KeyboardMobileUtils {

    public IOSDriver getDriver() {
        return (IOSDriver) super.getDriver();
    }

    @Override
    public void hideKeyboard() {
        if (getDriver().isKeyboardShown()) {
            log.info("Hiding IOS keyboard");
            getDriver().findElement(MobileBy.iOSClassChain("**/XCUIElementTypeButton[`label == 'Done'`]")).click();
        }
    }

    @Override
    public void tapPhoneBackButton() {
        throw new UnsupportedOperationException("Tapping physical phone button is not supported for iOS.");
    }
}
