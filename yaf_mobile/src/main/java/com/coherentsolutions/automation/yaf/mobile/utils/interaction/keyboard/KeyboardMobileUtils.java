package com.coherentsolutions.automation.yaf.mobile.utils.interaction.keyboard;

import com.coherentsolutions.automation.yaf.mobile.utils.MobileUtils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Dimension;
import org.springframework.stereotype.Component;

import static io.appium.java_client.touch.offset.PointOption.point;

@Component
@Slf4j
public abstract class KeyboardMobileUtils extends MobileUtils {

    public abstract void hideKeyboard();

    public void pullDownToRefresh() {
        log.info("Pulling down to refresh");
        AppiumDriver driver = getDriver();
        Dimension windowSize = driver.manage().window().getSize();
        int startX = windowSize.getWidth() / 2;
        int startY = (int) (0.2 * windowSize.getHeight());
        int endX = 0;
        int endY = (int) (0.8 * windowSize.getHeight()) - startY;
        (new TouchAction(driver))
                .press(point(startX, startY))
                .moveTo(point(endX, endY))
                .release()
                .perform();
    }

    public abstract void tapPhoneBackButton();

}
