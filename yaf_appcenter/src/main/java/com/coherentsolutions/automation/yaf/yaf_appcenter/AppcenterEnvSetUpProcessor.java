package com.coherentsolutions.automation.yaf.yaf_appcenter;

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.processor.start.env_setup.EnvSetupProcessor;
import com.coherentsolutions.automation.yaf.utils.store.FilesStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@Service
@Slf4j
@ConditionalOnProperty(name = "yaf.appcenter.enabled", havingValue = "true")
public class AppcenterEnvSetUpProcessor implements EnvSetupProcessor {

    @Autowired
    AppcenterProperties properties;

    @Autowired
    AppcenterService appcenterService;

    @Autowired
    FilesStore filesStore;

    @Override
    public boolean canHandle(EnvItem envItem) {
        return properties.getSupportedDeviceTypes().contains(envItem.getType());
    }

    @Override
    @Cacheable(key = "#envItem")
    public Map<String, Object> prepareEnvItem(EnvItem envItem) {
        Map<String, String> appInfo = envItem.getAppInfo();

        String type = "xxx";//todo get from appInfo
        String fileName = "yyy";
        try {
            File app = filesStore.getFile(type, fileName);
            if (app == null || !app.exists()) {
                app = appcenterService.downloadAppRelease("1", "2", filesStore.getFilePathToStore(type, fileName));
            }
            //build capabilities or similar

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
