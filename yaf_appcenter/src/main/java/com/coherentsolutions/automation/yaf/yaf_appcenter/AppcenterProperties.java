package com.coherentsolutions.automation.yaf.yaf_appcenter;

import com.coherentsolutions.automation.yaf.config.yaf.ModuleProperties;
import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = Consts.FRAMEWORK_NAME + ".appcenter")
public class AppcenterProperties extends ModuleProperties {

    String url;
    String token;
    String owner;
    List<DeviceType> supportedDeviceTypes;


}
