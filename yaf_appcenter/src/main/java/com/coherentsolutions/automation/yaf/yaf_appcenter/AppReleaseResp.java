package com.coherentsolutions.automation.yaf.yaf_appcenter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppReleaseResp {
    Integer id;
    @JsonProperty("short_version")
    String version;
    @JsonProperty("version")
    String versionId;
    @JsonProperty("uploaded_at")
    Date uploadedAt;
    Boolean enabled;
}
