## Publish framework : todo

- versioning
    * guide
    * bom
    *
- javadoc
    * write
    * grab and publish
    *
- documentation (asciidoc)
    * per module
    * global html
    * pdf
    * somehow grab all extra libs
    * publish to s3 (or similar)
    * 
- licencing (each file)
    * select proper licence
    * create a template?
    *
- checkstyle
    * common
    * custom linter
- unit tests
- contributors guide
- samples

 