## YAF Beans

YAF provides additional DI mechanism to work with beans according env or test context.

This is done to achieve various behaviour for Page Objects, Components, Utilities, DataObjects and other beans that you create.

### How it works

Before test method invocation YAF inits class fields according context with the help of `YafBeanProcessor`. It finds all fields that are marked as YAF Bean,
it finds proper bean implementation.
To init different types of beans YAF use `YafBeanFactory` implementations, for now there 2 types of beans that could be inited:

- Conditional bean : bean that has several implementations for different types of env. E.g. AndroidDeviсeUtils/IPhoneDeviceUtils
- Data bean : bean that prepare data pojo based on test context (metadata)

#### Conditional Beans

To create a bean that will be created depending on *env* context:

- Use one of the predefined annotations, like `@Android`, '@IOS', '@Chrome' (all of them you can find at `condition` package int the desired module)
- Or create your own by "extending" `@YafCondition` 
```java
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.TYPE})
    @Inherited
    @Documented    
    @YafCondition(mobileOs = MobileOS.ANDROID, // set your conditions here)
    public @interface CustomCondition {
        //
    }       
```
- Create a base class with common login, and extend it from Page/Utils/... or implement YafBeanInterface
- Create other implementations of this class and annotate them 
```java
    @Component
    @YourCustomCondition
```   

Find more details - `BaseYafBeanFactory`

#### Data Beans

Data bean is a POJO class, that will be loaded (deserialized) from a specific file. File name can be specified explicitly or calculate dynamically;

To create a data bean :

- Create a POJO class
- Annotate it 
```java
    @YafData()
    public class TestData {
        //...
    }
```
- Defaults: JSON data format is supported by default, if you want to deserialized another date format - set `reader` class in `YafData` annotation.
If you want to hardcode fileName to this class - you can specify `fileName` in annotation.
```java
    @YafData(reader = XmlDataReader.class, fileName = "xxx")
``` 

Find more details - `DataFactory`
 