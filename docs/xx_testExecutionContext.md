## Test Execution Context

Test Execution Context (`TEC`) - is one of the fundamentals of YAF.

TEC is a treadlocal bean, and it creates on test class creation, and handles all test method executions;

What data holds TEC:

- Test execution information (name, desc, suite)
- System info (start time, params, drivers)
- System test info (test method, class, execution params, annotations)  