package com.coherentsolutions.automation.yaf.selenide;

import com.coherentsolutions.automation.yaf.bean.pom.factory.YafLocatorFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Component
@Scope(SCOPE_PROTOTYPE)
@Primary
public class YafSelenideLocatorFactory extends YafLocatorFactory {

    public ElementLocator createLocator(Field field) {
        if (field.getType().toString().contains("SelenideElement")) {
            return null;
        }
        return new DefaultElementLocator(searchContext, field);
    }

}