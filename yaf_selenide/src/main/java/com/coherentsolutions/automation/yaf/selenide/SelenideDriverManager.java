package com.coherentsolutions.automation.yaf.selenide;

import com.codeborne.selenide.WebDriverRunner;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.events.driver.DriverStartEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SelenideDriverManager {


    @EventListener
    public void setDriverToSelenide(DriverStartEvent event) {
        DriverHolder holder = event.getDriverHolder();
        if (holder.getDeviceType().equals(DeviceType.WEB)) {
            log.debug("Setting webdriver to selenide runner");
            WebDriverRunner.setWebDriver(holder.getDriver());
        }
    }
}
