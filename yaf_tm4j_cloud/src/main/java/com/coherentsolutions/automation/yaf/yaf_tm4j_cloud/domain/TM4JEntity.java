package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class TM4JEntity {

    Integer id;
    String name;
    String projectKey;
}
