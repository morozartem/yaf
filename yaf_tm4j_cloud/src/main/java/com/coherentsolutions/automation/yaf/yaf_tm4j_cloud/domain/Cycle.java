package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString(callSuper = true)
public class Cycle extends TM4JEntity {


    String key;
    String description;
    IdValue project;
    IdValue jiraProjectVersion;
    IdValue status;
    IdValue folder;
    Map<String, Object> customFields;

}
