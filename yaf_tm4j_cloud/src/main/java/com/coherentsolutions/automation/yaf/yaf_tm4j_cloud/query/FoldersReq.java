package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.query;


import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain.FolderType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(chain = true)
public class FoldersReq extends PagingReq {

    FolderType folderType;

    @Override
    protected void appendProperParams(Map<String, String> params) {
        if (folderType != null) {
            params.put("folderType", folderType.getValue());
        }
    }
}
