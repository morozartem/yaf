package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString(callSuper = true)
public class TestExecution extends TM4JEntity {

    @JsonProperty("key")
    String name;
    IdValue project;
    IdValue testCase;
    IdValue environment;
    IdValue testExecutionStatus;
    Date actualEndDate;
    String comment;
    Boolean automated;
    IdValue testCycle;
    Map<String, Object> customFields;

}
