package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud;

import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.enums.TestResult;
import com.coherentsolutions.automation.yaf.events.global.ExecutionStartEvent;
import com.coherentsolutions.automation.yaf.events.test.TestFinishEvent;
import com.coherentsolutions.automation.yaf.test.YafTest;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain.TestExecution;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain.TestExecutionCreateDTO;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain.TestStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.util.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@Slf4j
@ConditionalOnProperty(name = Consts.FRAMEWORK_NAME + ".tm4jcloud.enabled", havingValue = "true")
public class TM4JCloudService {

    @Autowired
    PropertiesUtil propertiesUtil;

    @Autowired
    TM4JCloudProperties tm4JCloudProperties;

    @Autowired
    TM4JCloudClient cloudClient;

    String cycleKey;

    //TODO!!! may be some functional way?
    @EventListener
    public void createTm4jCycle(ExecutionStartEvent executionStartEvent) {
//        //we need to prepare proper folder/cycle/etc
//        String testFolder = ParamUtils.tryToGetParameter("TM4JTestFolder", null);
//        String state = suite.getParameter(ServiceWords.VSTATE);
//        String env = System.getProperty("environment");
//
//        String currentSuite = suite.getName();
//        Folder currentRunFolder = cloudClient.createFolderIfNotExist(testFolder, null, FolderType.TESTCYCLE);
//
//        CycleCreateDTO cycle = new CycleCreateDTO();
//        cycle.setName(currentSuite + " " + new SimpleDateFormat("dd_MM__HH_mm").format(new Date()));
//        cycle.setFolderId(currentRunFolder.getId());
//        cycle.setDescription(String.valueOf(suite.getXmlSuite().getParameters()));
//
//        //todo somehow add version state
//        Map<String, Object> cycleParams = new HashMap<>();
//        cycleParams.put("env", env);
//
//
//
//        System.out.println("Params " + cycleParams);
//        cycle.setCustomFields(cycleParams);
//
//        Cycle cycleResp = cloudClient.getCyclesRequest().create(cycle);
//        cycleKey = cloudClient.getCyclesRequest().get(cycleResp.getId()).getKey();

    }

    @EventListener
    public void logTestExecution(TestFinishEvent testFinishEvent) {
        YafTest yafTest = testFinishEvent.getTestInfo().getYafTest();
        if (yafTest != null && yafTest.tmsIds().length > 0) {
            Arrays.stream(yafTest.tmsIds()).forEach(testKeysId -> {
                updateTestExecution(testKeysId, testFinishEvent.getTestResult());
            });
        }
    }

    protected void updateTestExecution(String testKeysId, TestResult testResult) {
        TestExecutionCreateDTO testExecution = new TestExecutionCreateDTO();
        testExecution.setTestCaseKey(testKeysId);
        testExecution.setTestCycleKey(cycleKey);

        testExecution.setExecutionTime(testResult.getExecutionTime());
        TestStatus status;
        switch (testResult.getState()) {
            case SUCCESS: {
                status = TestStatus.PASS;
                break;
            }
            case FAIL: {
                status = TestStatus.FAIL;
                break;
            }
            default: {
                status = TestStatus.NOT_EXECUTE;
                break;
            }
        }

        testExecution.setStatusName(status);

        if (!testResult.isSuccess()) {
            testExecution.setComment(ExceptionUtils.getStackTrace(testResult.getError()));
        }
        TestExecution execution = cloudClient.getExecutionsRequest().create(testExecution);
        log.info("Test execution successfully added [{}] for {}", execution.getId(), testKeysId);
    }
}
