package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.query;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(chain = true)
public class CyclesReq extends PagingReq {

    Integer folderId;

    @Override
    protected void appendProperParams(Map<String, String> params) {
        if (folderId != null) {
            params.put("folderId", String.valueOf(folderId));
        }
    }
}
