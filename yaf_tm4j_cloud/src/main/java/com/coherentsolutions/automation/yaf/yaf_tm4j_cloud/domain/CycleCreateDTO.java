package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CycleCreateDTO extends Cycle {

    @JsonProperty("jiraProjectVersion")
    int projectVersion;
    Integer folderId;
}
