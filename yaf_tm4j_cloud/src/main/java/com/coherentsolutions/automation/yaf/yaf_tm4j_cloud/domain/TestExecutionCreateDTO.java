package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString(callSuper = true)
public class TestExecutionCreateDTO extends TestExecution {


    String projectKey;
    String testCaseKey;
    String testCycleKey;
    TestStatus statusName;
    List<TestScriptResult> testScriptResults;
    String environmentName;
    Long executionTime;

    @Data
    @Accessors(chain = true)
    public static class TestScriptResult {

        TestStatus statusName;
        Date actualEndDate;
        String actualResult;
    }

}
