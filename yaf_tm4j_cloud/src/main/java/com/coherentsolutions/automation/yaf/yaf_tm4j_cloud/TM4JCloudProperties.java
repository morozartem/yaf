package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud;

import com.coherentsolutions.automation.yaf.config.yaf.ModuleProperties;
import com.coherentsolutions.automation.yaf.consts.Consts;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = Consts.FRAMEWORK_NAME + ".tm4j.cloud")
public class TM4JCloudProperties extends ModuleProperties {

    String projectKey;
    String apiKey;
    String tm4jUrl = "https://api.adaptavist.io/tm4j/v2/";
}
