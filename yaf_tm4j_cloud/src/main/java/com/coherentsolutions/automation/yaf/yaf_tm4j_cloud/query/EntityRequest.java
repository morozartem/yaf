package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.query;

import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain.TM4JEntity;
import io.restassured.specification.RequestSpecification;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static io.restassured.RestAssured.given;

public class EntityRequest<T extends TM4JEntity, K extends PagingReq> {

    RequestSpecification specification;
    Class entityClass;
    String projectKey;
    String url;

    public EntityRequest(RequestSpecification specification, Class entityClass, String url, String projectKey) {
        this.specification = specification;
        this.entityClass = entityClass;
        this.url = url;
        this.projectKey = projectKey;
    }


    public List<T> query(K pagingReq) {
        Map<String, String> params = pagingReq.getQueryParams();
        if (projectKey != null) {
            params.put("projectKey", projectKey);
        }
        return given(specification)
                .params(params)
                .get(url)
                .jsonPath().getList("values", entityClass);
    }

    public T find(K pagingReq, String name) {
        return find(pagingReq, (f) -> f.getName().contains(name));
    }

    public T find(K pagingReq, Predicate<T> p) {
        T res = null;
        List<T> list = null;
        do {
            try {
                list = query(pagingReq);
                res = list.stream().filter(p).findFirst().orElse(null);
                pagingReq.setStartAt(pagingReq.getStartAt() + pagingReq.getMaxResults());
            } catch (Exception ex) {
                //workaround to handle out of bounds reqs
                return null;
            }
        } while (res == null && !list.isEmpty());
        return res;
    }

    public T get(Integer id) {
        return (T) given(specification).get(url + "/" + id).as(entityClass);
    }

    public T create(T obj) {
        obj.setProjectKey(projectKey);
        Integer id = given(specification).body(obj).post(url).jsonPath().getInt("id");
        obj.setId(id);
        return obj;
    }


}
