package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud;

import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain.Cycle;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain.Folder;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain.FolderType;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain.TestExecution;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.query.CyclesReq;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.query.EntityRequest;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.query.FoldersReq;
import com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.query.TestExecutionReq;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TM4JCloudClient {

    @Autowired
    TM4JCloudProperties tm4JCloudProperties;

    @Getter
    EntityRequest<Folder, FoldersReq> foldersRequest;
    @Getter
    EntityRequest<Cycle, CyclesReq> cyclesRequest;
    @Getter
    EntityRequest<TestExecution, TestExecutionReq> executionsRequest;


    public TM4JCloudClient() {

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        RequestSpecification specification = new RequestSpecBuilder()
                .addHeader("Authorization", "Bearer " + tm4JCloudProperties.getApiKey())
                .setContentType(ContentType.JSON)
                .setBaseUri(tm4JCloudProperties.getTm4jUrl())
                .addFilter(new ResponseLoggingFilter())
                .build();

        String projectKey = tm4JCloudProperties.getProjectKey();
        foldersRequest = new EntityRequest<>(specification, Folder.class, "folders", projectKey);
        cyclesRequest = new EntityRequest<>(specification, Cycle.class, "testcycles", projectKey);
        executionsRequest = new EntityRequest<>(specification, TestExecution.class, "testexecutions", projectKey);

    }

    public Folder createFolderIfNotExist(String name, Integer parentId, FolderType type) {
        EntityRequest<Folder, FoldersReq> foldersReq = getFoldersRequest();
        FoldersReq cycleFolderReq = new FoldersReq().setFolderType(type);
        Folder folder = foldersReq.find(cycleFolderReq, (f) ->
                f.getName().equals(name) && f.getParentId() == parentId
        );

        if (folder == null) {
            //create new folder
            folder = new Folder();
            folder.setName(name);
            folder.setParentId(parentId);
            folder.setFolderType(type);
            folder = foldersReq.create(folder);
        }
        if (folder.getId() == null) {
            throw new IllegalArgumentException("Could not find/crete folder with name " + name + " and parent id " + parentId);
        }
        return folder;
    }

}
