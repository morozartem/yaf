package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.domain;

import lombok.Data;

@Data
public class IdValue {

    Integer id;
    String self;
}