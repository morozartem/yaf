package com.coherentsolutions.automation.yaf.yaf_tm4j_cloud.query;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(chain = true)
public class TestExecutionReq extends PagingReq {

    String testCycle;
    String testCase;

    @Override
    protected void appendProperParams(Map<String, String> params) {
        if (testCycle != null) {
            params.put("testCycle", testCycle);
        }
        if (testCase != null) {
            params.put("testCase", testCase);
        }
    }
}
