package com.coherentsolutions.automation.yaf.yaf_bootstrap.steps;

import com.coherentsolutions.automation.yaf.yaf_bootstrap.ConfigStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.FrameConfig;
import lombok.Getter;

public class StringValueStep extends ConfigStep {

    @Getter
    String userText;
    String paramName;

    public StringValueStep(FrameConfig config, String userText, String paramName) {
        super(config);
        this.userText = userText;
        this.paramName = paramName;
    }

    @Override
    public ConfigStep process(Object input) {
        config.put(paramName, input);
        return getNextStep();
    }


}
