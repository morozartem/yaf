package com.coherentsolutions.automation.yaf.yaf_bootstrap;

import com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.BooleanValueStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.ListValueStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.MultiSelectListValueStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.StringValueStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.integartion.AppcenterConfig;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.integartion.TM4JConfig;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.reporting.AllureConfig;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.web.BSConfig;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.web.SLConfig;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Arrays;
import java.util.Scanner;

@Data
@Accessors(chain = true)
public class Wizard {

    static FrameConfig<String, Object> config = new FrameConfig();


    public static ConfigStep buildConfig2() {
        ConfigStep browsers = new MultiSelectListValueStep(config, "Please select browser", "frame.web.browsers", Arrays.asList("Chrome", "FireFox", "Safari", "Edge", "IE", "Opera"));

        //mobile
        ConfigStep mobileStep = new BooleanValueStep(config, "Will u use mobile testing in your frame", "frame.mobile");
        browsers.withNextStep(mobileStep);

        return browsers;
    }

    public static ConfigStep buildConfig() {


        ConfigStep configStep = new StringValueStep(config, "pls select frame name", "frame.name");
        //web
        ConfigStep webStep = new BooleanValueStep(config, "Will u use web testing in your frame", "frame.web");


        ConfigStep webCloudsStep = new BooleanValueStep(config, "Will u use web clouds in your frame", "frame.web.clouds");


        ConfigStep webCloudsListStep = new ListValueStep(config, "Will u use web clouds in your frame", "frame.web.cloud.name",
                Arrays.asList("BrowserStack", "SauceLabs"));

        ConfigStep bsConfig = new BSConfig(config);
        ConfigStep slConfig = new SLConfig(config);

        ConfigStep webFramework = new ListValueStep(config, "Select web framework to use:", "frame.web.framework", Arrays.asList("Selenide", "None"));

        ConfigStep browsers = new MultiSelectListValueStep(config, "Please select browser", "frame.web.browsers", Arrays.asList("Chrome", "FireFox", "Safari", "Edge", "IE", "Opera"));

        //mobile
        ConfigStep mobileStep = new BooleanValueStep(config, "Will u use mobile testing in your frame", "frame.mobile");

        //desktop
        ConfigStep desktopStep = new BooleanValueStep(config, "Will u use desktop testing in your frame", "frame.desktop");


        //integration
        ConfigStep tm4j = new BooleanValueStep(config, "TM4J is ok for u?", "frame.integaration.tm4j");

        ConfigStep tm4jConfig = new TM4JConfig(config);

        //if(config.get("frame.mobile").equals("1") || config.get("frame.desktop").equals("1")) {
        ConfigStep appCenter = new BooleanValueStep(config, "Appcenter is ok for u?", "frame.integaration.appcenter");
        ConfigStep appCenterConfig = new AppcenterConfig(config);
        //}


        //reporting
        ConfigStep allure = new BooleanValueStep(config, "Allure is ok for u?", "frame.reporting.allure");
        AllureConfig allureConfig = new AllureConfig(config);


        /// mapping steps
        configStep.withNextStep(webStep);
        webStep.withTruFalseSteps(webCloudsStep, mobileStep);
        webCloudsStep.withTruFalseSteps(webCloudsListStep, webFramework);
        webCloudsListStep.withNextSteps(Arrays.asList(bsConfig, slConfig));
        bsConfig.withNextStep(webFramework);
        slConfig.withNextStep(webFramework);
        webFramework.withNextStep(browsers);
        browsers.withNextStep(mobileStep);

        mobileStep.withTruFalseSteps(null, desktopStep);
        desktopStep.withTruFalseSteps(null, tm4j);

        tm4j.withTruFalseSteps(tm4jConfig, appCenter);
        tm4jConfig.withNextStep(appCenter);

        appCenter.withTruFalseSteps(appCenterConfig, allure);
        appCenterConfig.withNextStep(allure);

        allure.withTruFalseSteps(allureConfig, null);
        allureConfig.withNextStep(null);

        return configStep;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ConfigStep configStep = buildConfig();
        String lastClientText;
        String s = null;
        do {
            lastClientText = configStep.getUserText();
            System.out.println(lastClientText);
            Object input;
            do {
                s = scanner.nextLine();
                input = configStep.validateInput().apply(s);
                if (input == null) {
                    System.out.println("make one more attempt");
                    System.out.println(lastClientText);
                }
            } while (input == null);
            System.out.println(config);
            configStep = configStep.process(input);

            if (configStep == null) {
                System.out.println("Thanks!");
                System.out.println(config);
                return;
            }
        } while (!"quit".equals(s));
    }
}
