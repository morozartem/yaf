package com.coherentsolutions.automation.yaf.yaf_bootstrap.steps;

import com.coherentsolutions.automation.yaf.yaf_bootstrap.ConfigStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.FrameConfig;

import java.util.ArrayList;
import java.util.List;


public class MultiSelectWizardStep extends ListValueStep {

    List<String> selected = new ArrayList<>();
    ConfigStep currentSubStep;

    int currentOptionIndex;

    public MultiSelectWizardStep(FrameConfig config, String userText, String paramName, List<String> options) {
        super(config, userText, paramName, options);
    }

    @Override
    public String getUserText() {
        return super.getUserText() + " " + options.get(currentOptionIndex);
    }

    @Override
    public ConfigStep process(Object input) {

        ConfigStep nestedStep = getNextStep(currentOptionIndex);


        if (currentSubStep == null) {
            //handle general list
            Integer ii = (Integer) input;
            ConfigStep resp = getNextStep(ii);
            currentSubStep = resp;
        } else {
            ConfigStep resp = currentSubStep.process(input);
            if (resp != null) {
                currentSubStep = resp;
                return currentSubStep;
            } else {
                //we achieve end of sub wizard
            }
        }

        Integer ii = (Integer) input;
        if (ii == 0) {
            //exit
            config.put(paramName, options.get(ii));
            return getNextStep();
        } else {
            Object resp = getNextStep(ii).process(input);
            return this;
        }
    }


    @Override
    protected boolean shouldIncludeZero() {
        return true;
    }
}
