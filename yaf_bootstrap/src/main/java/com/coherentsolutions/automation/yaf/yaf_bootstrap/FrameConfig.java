package com.coherentsolutions.automation.yaf.yaf_bootstrap;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.HashMap;

@Data
@ToString
@Accessors(chain = true)
public class FrameConfig<K, V> extends HashMap {

//    public void put(String key, Object value){
//
//    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        keySet().forEach(k -> sb.append(k + ":" + get(k) + "\n"));
        return sb.toString();
    }
}
