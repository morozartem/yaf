package com.coherentsolutions.automation.yaf.yaf_bootstrap.steps;

import com.coherentsolutions.automation.yaf.yaf_bootstrap.ConfigStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.FrameConfig;
import lombok.Getter;

import java.util.List;
import java.util.function.Function;

public class ListValueStep extends ConfigStep {

    @Getter
    String userText;
    String paramName;
    List<String> options;

    public ListValueStep(FrameConfig config, String userText, String paramName, List<String> options) {
        super(config);
        this.userText = userText;
        this.paramName = paramName;
        this.options = options;
    }

    protected boolean shouldIncludeZero() {
        return false;
    }

    @Override
    public ConfigStep process(Object input) {
        Integer ii = (Integer) input;
        config.put(paramName, options.get(ii));
        if (stepsSize() > 1) {
            //select proper step for every option
            return getNextStep(ii);
        } else {
            return getNextStep();
        }
    }

    @Override
    public Function<String, Object> validateInput() {
        return (input) -> {
            Integer ii = Integer.parseInt(input);
            if (ii > (shouldIncludeZero() ? -1 : 0) && ii <= options.size()) {
                return ii - 1;
            }
            return null;
        };
    }

    public String getUserText() {
        return userText + "\n" + prepareList(options);
    }
}
