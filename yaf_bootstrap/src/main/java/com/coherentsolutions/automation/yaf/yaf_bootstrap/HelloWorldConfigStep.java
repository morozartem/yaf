package com.coherentsolutions.automation.yaf.yaf_bootstrap;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

@Data
@Accessors(chain = true)
public class HelloWorldConfigStep extends ConfigStep {
    List<String> lst = Arrays.asList("one", "two", "three");
    String state = null;

    public HelloWorldConfigStep(FrameConfig frameConfig) {
        super(frameConfig);
    }

    @Override
    public ConfigStep process(Object input) {
        String ss = getListItem((String) input, lst);
        state = ss;
        return this;
    }

    @Override
    public Function<String, Object> validateInput() {
        return (s) -> {
            Integer ss = Integer.parseInt(s);
            if (ss > 0 && ss <= lst.size()) {
                return ss;
            }
            return null;
        };
    }

    @Override
    public String getUserText() {
        return "Select (" + state + ") \n" + prepareList(lst);
    }
}
