package com.coherentsolutions.automation.yaf.yaf_bootstrap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public abstract class ConfigStep {

    protected FrameConfig config;
    List<ConfigStep> nextSteps;
    Map<String, Object> dependsOn;

    public ConfigStep(FrameConfig config) {
        this.config = config;
    }

    public ConfigStep withDepends(String key, Object value) {
        if (dependsOn == null) {
            dependsOn = new HashMap<>();
        }
        dependsOn.put(key, value);
        return this;
    }

    public abstract ConfigStep process(Object input);

    public abstract String getUserText();

    protected String prepareList(List lst) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lst.size(); i++) {
            sb.append((i + 1) + ". " + lst.get(i) + "\n");
        }
        return sb.toString();
    }

    protected <T> T getListItem(String input, List lst) {
        try {
            Integer index = Integer.parseInt(input);
            return (T) lst.get(index - 1);
        } catch (Exception e) {
            return null;
        }
    }

    public Function<String, Object> validateInput() {
        return (input) -> input;
    }

    public void withNextSteps(List<ConfigStep> steps) {
        this.nextSteps = steps;
    }

    public void withNextStep(ConfigStep step) {
        this.nextSteps = Arrays.asList(step);
    }

    protected ConfigStep getNextStep() {
        return getNextStep(0);
    }

    protected ConfigStep getNextStep(int index) {
        //validate
        return nextSteps.get(index);
    }

    public void withTruFalseSteps(ConfigStep trueStep, ConfigStep falseSteps) {
        this.nextSteps = Arrays.asList(trueStep, falseSteps);
    }

    protected int stepsSize() {
        return nextSteps.size();
    }

}
