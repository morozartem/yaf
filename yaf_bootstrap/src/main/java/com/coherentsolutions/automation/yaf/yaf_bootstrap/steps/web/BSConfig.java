package com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.web;

import com.coherentsolutions.automation.yaf.yaf_bootstrap.ConfigStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.FrameConfig;
import lombok.Getter;

import java.util.Scanner;

public class BSConfig extends ConfigStep {

    @Getter
    String userText = "enter api key";

    public BSConfig(FrameConfig config) {
        super(config);
    }

    @Override
    public ConfigStep process(Object input) {
        config.put("bs.apikey", input);
        Scanner scanner = new Scanner(System.in);
        System.out.println("input user name");
        config.put("bs.user", scanner.nextLine());
        System.out.println("input user pass");
        config.put("bs.pass", scanner.nextLine());
        return getNextStep();
    }

}
