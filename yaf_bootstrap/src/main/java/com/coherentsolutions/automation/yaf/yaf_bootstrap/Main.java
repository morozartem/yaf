package com.coherentsolutions.automation.yaf.yaf_bootstrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        //name of the project
        //will you project test web
        // clouds
        // browsers
        // frameworks
        //mobile

        // data

        //integrations

        //reporting
    }

    public static void main2(String[] args) {
        SpringApplication.run(Main.class);
    }

    @ShellComponent
    public class MyCommands {

        @ShellMethod("Add two integers together.")
        public int add(int a, int b) {
            return a + b;
        }

        @ShellMethod("Add2 two integers together.")
        public List<String> lst() {
            return Arrays.asList("d32", "34434", "fwef2");
        }
    }

}
