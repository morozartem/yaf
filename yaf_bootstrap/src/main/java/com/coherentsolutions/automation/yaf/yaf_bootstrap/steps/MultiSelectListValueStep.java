package com.coherentsolutions.automation.yaf.yaf_bootstrap.steps;

import com.coherentsolutions.automation.yaf.yaf_bootstrap.ConfigStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.FrameConfig;

import java.util.ArrayList;
import java.util.List;


public class MultiSelectListValueStep extends ListValueStep {

    List<String> selected = new ArrayList<>();


    public MultiSelectListValueStep(FrameConfig config, String userText, String paramName, List<String> options) {
        super(config, userText, paramName, options);
    }

    @Override
    public String getUserText() {
        return super.getUserText() + "0. Stop selecting";
    }

    @Override
    public ConfigStep process(Object input) {
        Integer ii = (Integer) input;
        if (ii == -1) {
            //exit
            config.put(paramName, selected);
            return getNextStep();
        } else {
            selected.add(options.get(ii));
            return this;
        }
    }

    @Override
    protected boolean shouldIncludeZero() {
        return true;
    }
}
