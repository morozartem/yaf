package com.coherentsolutions.automation.yaf.yaf_bootstrap.steps.web;

import com.coherentsolutions.automation.yaf.yaf_bootstrap.ConfigStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.FrameConfig;

import java.util.function.Function;

public class GeneralWebStep extends ConfigStep {

    public GeneralWebStep(FrameConfig frameConfig) {
        super(frameConfig);
    }

    @Override
    public ConfigStep process(Object input) {
        return null;
    }

    @Override
    public Function<String, Object> validateInput() {
        return null;
    }

    @Override
    public String getUserText() {
        return "Will u test web?";
    }
}
