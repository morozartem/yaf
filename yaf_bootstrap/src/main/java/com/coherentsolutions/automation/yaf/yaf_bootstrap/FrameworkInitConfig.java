package com.coherentsolutions.automation.yaf.yaf_bootstrap;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@Accessors(chain = true)
public class FrameworkInitConfig {

    String name;
    List<String> types;
    Map<String, String> properties;
}
