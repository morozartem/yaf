package com.coherentsolutions.automation.yaf.yaf_bootstrap.steps;

import com.coherentsolutions.automation.yaf.yaf_bootstrap.ConfigStep;
import com.coherentsolutions.automation.yaf.yaf_bootstrap.FrameConfig;
import lombok.Getter;

import java.util.function.Function;

public class BooleanValueStep extends ConfigStep {

    @Getter
    String userText;
    String paramName;

    public BooleanValueStep(FrameConfig config, String userText, String paramName) {
        super(config);
        this.userText = userText;
        this.paramName = paramName;
    }

    @Override
    public ConfigStep process(Object input) {
        config.put(paramName, input);
        if ((Boolean) input) {
            return getNextStep(0);
        }
        return getNextStep(1);
    }

    public String getUserText() {
        return userText + " [1 - true]";
    }

    @Override
    public Function<String, Object> validateInput() {
        return (input) -> "1".equals(input) ? true : false;

    }
}
