package com.coherentsolutions.automation.yaf.processor.start.env_setup;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.Env;
import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.events.test.TestStartEvent;
import com.coherentsolutions.automation.yaf.listener.YafListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class EnvSetUpService extends YafListener {

    //TODO!!

    Map<EnvItem, Map<String, Object>> preparedEnvs = new HashMap<>();
    @Autowired(required = false)
    List<EnvSetupProcessor> envSetupProcessors;


    @EventListener
    @Order(2)
    public void testStart(TestStartEvent event) {
        if (envSetupProcessors != null) {
            TestExecutionContext testExecutionContext = getTestExecutionContext();
            Env env = testExecutionContext.getEnv();
            List<EnvItem> unpreparedEnvs = new ArrayList<>();
            env.forEach(envItem -> {
                if (preparedEnvs.containsKey(envItem)) {
                    testExecutionContext.setParams(preparedEnvs.get(envItem));
                } else {
                    unpreparedEnvs.add(envItem);
                }
            });
            if (false) {
                if (!unpreparedEnvs.isEmpty()) {
                    ExecutorService WORKER_THREAD_POOL
                            = Executors.newFixedThreadPool(5);
                    CountDownLatch latch = new CountDownLatch(unpreparedEnvs.size());
                    unpreparedEnvs.forEach(envItem -> {
                        try {
                            Map<String, Object> preporatorParams = WORKER_THREAD_POOL.submit(() -> {

                                EnvSetupProcessor preporator = envSetupProcessors.stream().filter(p -> p.canHandle(envItem)).findFirst().orElse(null);
                                Map<String, Object> res = null;
                                if (preporator != null && envItem.getAppInfo() != null) {
                                    res = preporator.prepareEnvItem(envItem);
                                }
                                //prepapre e
                                latch.countDown();
                                return res;

                            }).get();
                            preparedEnvs.put(envItem, preporatorParams);
                            testExecutionContext.setParams(preporatorParams);
                        } catch (Exception e) {
                            //TODO
                        }
                    });
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
