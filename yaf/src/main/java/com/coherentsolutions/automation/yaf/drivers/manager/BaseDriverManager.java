package com.coherentsolutions.automation.yaf.drivers.manager;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.drivers.properties.DriverProperties;
import com.coherentsolutions.automation.yaf.enums.Scope;
import com.coherentsolutions.automation.yaf.exception.DriverYafException;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@ConditionalOnSingleCandidate(DriverManager.class)
public class BaseDriverManager implements DriverManager {

    @Autowired
    List<DriverResolver> resolvers;

    @Autowired
    private Environment env;

    @Autowired
    DriverProperties driverProperties;

    @Autowired
    WebDriverListener webDriverListener;

    @Override
    public DriverHolder getDriver(EnvItem envItem) throws DriverYafException {
        DriverResolver resolver = resolvers.stream().filter(r -> r.canResolve(envItem)).findFirst().orElse(null);
        if (resolver != null) {
            DriverHolder holder = resolver.initDriver(envItem);
            if (holder != null) {
                holder.setScope(getDriversScope(envItem, resolver));
                if (holder.getDeviceType() == null) {
                    throw new DriverYafException("XXXXX " + envItem.getName());
                }
                if (driverProperties.isPublishDriverEvents()) {
                    EventFiringWebDriver efwd = new EventFiringWebDriver(holder.getDriver());
                    efwd.register(webDriverListener);
                    holder.setDriver(efwd);
                }
                return holder;
            }
        }
        throw new DriverYafException("Unable to init proper driver for env " + envItem.getName());
    }

    protected Scope getDriversScope(EnvItem envItem, DriverResolver resolver) {
        Scope scope = envItem.getScope();
        if (scope == null) {
            String resolverType = resolver.getResolverType().name();
            String val = env.getProperty(resolverType + ".driver.scope");
            if (val == null) {
                val = env.getProperty("driver.scope", "execution");
            }
            scope = Scope.valueOfName(val);
        }
        return scope;
    }
}
