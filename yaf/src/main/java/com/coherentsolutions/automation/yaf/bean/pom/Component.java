package com.coherentsolutions.automation.yaf.bean.pom;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.bean.YafBean;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.wait.driver.DriverWaitService;
import com.coherentsolutions.automation.yaf.wait.driver.WaitClassUtils;
import com.coherentsolutions.automation.yaf.wait.driver.WaitFor;
import lombok.Getter;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.Map;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

//todo think about webdriver ex in thic class and waits in general

@org.springframework.stereotype.Component
@Scope(SCOPE_PROTOTYPE)
public abstract class Component implements IContextual, YafBean {

    @Getter
    protected TestExecutionContext testExecutionContext;

    @Getter
    protected DriverHolder driverHolder;

    @Autowired
    protected DriverWaitService waitService;
    @Autowired
    private WaitClassUtils waitClassUtils;


    //TODO think about that!!! may be carete one more inteface and call it explicitly
    @Override
    public void setTestExecutionContext(TestExecutionContext testExecutionContext) {
        this.testExecutionContext = testExecutionContext;
        init();
    }

    public boolean isLoaded() {
        try {
            waitForLoad();
            return true;
        } catch (Exception ex) {//todo
            return false;
        }
    }

    public void waitForLoad() throws WebDriverException {
        Map<Object, WaitFor> waitForFieldsForLoad = waitClassUtils.getWaitForFieldsForLoad(this);
        if (!waitForFieldsForLoad.isEmpty()) {
            waitForFieldsForLoad.keySet().forEach(e -> {
                WebElement element = null;
                if (e instanceof List) {
                    List<WebElement> elements = (List<WebElement>) e;
                    //todo check
                    //seems that we already should wait all elements and calling one item will be enough
                    if (!elements.isEmpty()) {
                        element = elements.get(0);
                    }
                } else {
                    element = (WebElement) e;
                }
                if (element != null) {
                    element.getTagName();//just trigger proxy
                } else {
                    throw new WebDriverException("xxxxxx");
                }
            });
        }
    }


    protected abstract void init();
}
