package com.coherentsolutions.automation.yaf.utils;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */


import com.coherentsolutions.automation.yaf.config.env.Env;
import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.enums.*;
import com.coherentsolutions.automation.yaf.exception.GeneralYafException;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class YafBy extends By {

    Map<YafEnum, By> selectors;
    TestExecutionContext testExecutionContext;


    public YafBy(TestExecutionContext testExecutionContext) {
        this.testExecutionContext = testExecutionContext;
        selectors = new LinkedHashMap<>();
    }

    /*public MBy(List<By> selectors) {
        this.selectors = selectors;
    }*/

    private By findByAccordingEnv() throws GeneralYafException {
        //TODO double check in parallel execution
        //TestExecutionContext testExecutionContext = StaticContextAccessor.getBean(TestExecutionContext.class);
        Env env = testExecutionContext.getEnv();
        YafEnum e = selectors.keySet().stream().findFirst().get();
        EnvItem envItem = null;
        By by = null;
        try {
            if (e instanceof MobileOS) {
                envItem = env.findByType(DeviceType.MOBILE);
                by = selectors.get(envItem.getMobileOS());
            } else if (e instanceof Browser) {
                envItem = env.findByType(DeviceType.WEB);
                by = selectors.get(envItem.getBrowser());
            } else if (e instanceof OS) {
                envItem = env.findByType(DeviceType.DESKTOP);
                by = selectors.get(envItem.getOs());
            }
            return by;
        } catch (Exception ex) {
            throw new GeneralYafException("XXX22XX");
        }

    }

    @Override
    public List<WebElement> findElements(SearchContext context) {
        try {
            return context.findElements(findByAccordingEnv());
        } catch (GeneralYafException ex) {
            //TODO xx
            throw new WebDriverException("Could not find elements via selector " + this.toString());
        }
    }

    @Override
    public WebElement findElement(SearchContext context) {
        try {
            return context.findElement(findByAccordingEnv());
        } catch (GeneralYafException ex) {
            //TODO xx
            throw new WebDriverException("Could not find elements via selector " + this.toString());
        }
    }

    public YafBy add(YafEnum yafEnum, By by) {
        selectors.put(yafEnum, by);
        return this;
    }
}
