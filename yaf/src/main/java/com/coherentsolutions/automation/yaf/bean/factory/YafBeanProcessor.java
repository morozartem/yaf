package com.coherentsolutions.automation.yaf.bean.factory;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.exception.EnvSetupYafException;
import com.coherentsolutions.automation.yaf.utils.YafBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.List;

@Service
@Slf4j
public class YafBeanProcessor {


    @Autowired
    List<YafBeanFactory> factories;

    @Autowired
    YafBeanUtils beanUtils;

    @Autowired(required = false)
    List<YafBeanFieldProxyService> proxyServiceList;

    @Autowired
    ApplicationContext applicationContext;


    //todo recursion?!

    public void processBean(Object bean, TestExecutionContext testExecutionContext) {
        List<Field> fields = beanUtils.getClassYafFields(bean.getClass());
        fields.forEach(field -> {
            try {
                Class fieldType = field.getType();
                Object fieldValue = null;
                if (beanUtils.containsBean(field.getName())) {
                    fieldValue = getBean(fieldType, testExecutionContext);
                } else {
                    fieldValue = field.get(bean);
                }
                if (fieldValue != null) {

                    if (proxyServiceList != null) {
                        YafBeanFieldProxyService yafFieldProxy = proxyServiceList.stream().filter(p -> p.canProxy(field)).findFirst().orElse(null);
                        if (yafFieldProxy != null) {
                            fieldValue = yafFieldProxy.proxyField(field, fieldValue, bean);
                        }
                    }

                    field.set(bean, fieldValue);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                String message = String.format("Field %s of type %s is YafBean but could not be initialized.",
                        field.getName(), field.getType().getCanonicalName());
                throw new EnvSetupYafException(message, e);
            }
        });
    }

    public <T> T getBean(Class objType, TestExecutionContext testExecutionContext) {
        try {
            YafBeanFactory yafBeanFactory = factories.stream().filter(factory -> factory.isSupportedBeanType(objType)).findFirst().orElse(null);
            Object bean = yafBeanFactory.getBean(objType, testExecutionContext);
            processBean(bean, testExecutionContext);
            return (T) bean;
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("Error initializing bean for {}, cause {}", objType, ex.getMessage());
            return null;
        }
    }

}
