package com.coherentsolutions.automation.yaf.drivers.manager;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.processor.finish.test.log.LogProcessor;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class DriverResolver {

    @Autowired(required = false)
    LogProcessor loggingManager;

    public boolean canResolve(EnvItem envItem) {
        return envItem.getType().equals(getResolverType());
    }

    public abstract DriverHolder initDriver(EnvItem envItem);

    public abstract DeviceType getResolverType();


    protected boolean emptyFarm(EnvItem envItem) {
        return envItem.getFarm() == null;
    }

    protected boolean farmName(EnvItem envItem, String name) {
        return !emptyFarm(envItem) && envItem.getFarm().getName().equalsIgnoreCase(name);
    }


    protected DriverHolder buildHolder(EnvItem envItem, WebDriver webDriver) {
        DriverHolder driverHolder = new DriverHolder(envItem);
        driverHolder.setDriver(webDriver);
        driverHolder.setDeviceType(getResolverType());
        return driverHolder;
    }

    //TODO think about other types of drivers
    protected Capabilities buildCapabilitiesFromEnv(EnvItem envItem) {
        MutableCapabilities capabilities = new MutableCapabilities(envItem.getCapabilities());
        if (loggingManager != null) {
            LoggingPreferences loggingPreferences = loggingManager.getLoggingConfig();
            if (loggingPreferences != null) {
                capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);
            }
        }
        return capabilities;
    }


}
