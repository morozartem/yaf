package com.coherentsolutions.automation.yaf.context.test;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.Env;
import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.context.execution.ExecutionContext;
import com.coherentsolutions.automation.yaf.drivers.manager.DriverManager;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.drivers.model.DriversStore;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.enums.Scope;
import com.coherentsolutions.automation.yaf.events.test.TestStartEvent;
import com.coherentsolutions.automation.yaf.exception.DriverYafException;
import com.coherentsolutions.automation.yaf.test.model.TestInfo;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;



@SuppressWarnings("ALL")
@Data
@Accessors(chain = true)
@Component
@org.springframework.context.annotation.Scope(Consts.SCOPE_THREADLOCAL)
@Slf4j
public abstract class TestExecutionContext implements Serializable {

    protected String testName;
    protected String suiteName;

    protected Locale locale = Locale.getDefault();
    protected TestInfo testInfo;

    protected Long startTime;
    protected Env env;

    protected Map<String, Object> params;

    @Autowired
    protected ExecutionContext executionContext;

    @Autowired
    protected ApplicationContext applicationContext;

    @Autowired
    protected DriverManager driverManager;

    protected DriversStore threadDrivesStore;


    public TestExecutionContext() {
        log.debug("Building test context for thread {}", Thread.currentThread().getName());
        threadDrivesStore = new DriversStore();
        params = new HashMap<>();
    }

    protected abstract DriverHolder findDriverHolderInContext(EnvItem envItem);

    protected abstract void addDriverToContext(EnvItem envItem, DriverHolder driverHolder);

    protected abstract DriverHolder getDriverHolder(DeviceType deviceType, String... name) throws DriverYafException;

    public abstract DriverHolder getWebDriverHolder(String... name) throws DriverYafException;

    public abstract DriverHolder getMobileDriverHolder(String... name) throws DriverYafException;

    public abstract DriverHolder getDesktopDriverHolder(String... name) throws DriverYafException;

    public abstract void buildFromTestStartEvent(TestStartEvent event, Env env);

    public void addPrams(Map<String, Object> p) {
        params.putAll(p);
    }

    public void addStringParams(Map<String, String> stringParams) {
        params.putAll(stringParams.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));
    }

    public DriverHolder findInitedDriverHolderByType(DeviceType deviceType) {
        DriverHolder holder = threadDrivesStore.findInitedDriverByType(deviceType);
        if (holder == null) {
            //lets search in execution context
            holder = executionContext.findInitedDriverHolderByType(suiteName, deviceType);
        }
        return holder;
    }

    public Stream<Map.Entry<EnvItem, DriverHolder>> initedDriverHolders() {
        return threadDrivesStore.getDriverHolderMap().entrySet().stream();
    }

    protected void clearBeforeNewTest() {
        //currentTestState = null;
        //fixtures = null;
        startTime = null;
        testInfo = null;
        //remove all drivers from testMethodScope
        threadDrivesStore.clearAll(Scope.METHOD);
    }

    @PreDestroy
    public void clearContext() {
        log.debug("Clearing context");
        //we need to close all drivers
        threadDrivesStore.clearAll();
    }
}
