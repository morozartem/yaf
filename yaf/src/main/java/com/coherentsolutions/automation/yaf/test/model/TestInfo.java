package com.coherentsolutions.automation.yaf.test.model;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.test.YafTest;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
public class TestInfo {

    String testId;
    String testName;
    @Setter(value = AccessLevel.PRIVATE)
    String testMethodName;

    SuiteInfo suiteInfo;

    @Getter(value = AccessLevel.PRIVATE)
    @Setter(value = AccessLevel.PRIVATE)
    String fullTestName;
    Method testMethod;
    Class testClass;
    Map<String, Object> testMethodParams;
    Map<String, String> testParams;

    @Getter
    @Setter(value = AccessLevel.PRIVATE)
    List<Annotation> annotationList;


    YafTest yafTest;
    String envSetup;

    Object runnerContext;

    public String getFullTestName() {
        if (fullTestName == null) {
            fullTestName = testClass.getName() + "." + testMethodName + "[" + testName + "]";
        }
        return fullTestName;
    }

    public void setTestMethod(Method testMethod) {
        this.testMethod = testMethod;
        this.testMethodName = testMethod.getName();
        this.annotationList = Arrays.stream(testMethod.getDeclaredAnnotations()).collect(Collectors.toList());
    }
}
