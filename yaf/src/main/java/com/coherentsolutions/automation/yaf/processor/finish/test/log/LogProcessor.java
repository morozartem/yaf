package com.coherentsolutions.automation.yaf.processor.finish.test.log;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.events.test.TestFinishEvent;
import com.coherentsolutions.automation.yaf.processor.finish.test.TestFinishEventProcessor;
import com.coherentsolutions.automation.yaf.processor.finish.test.TestFinishProperties;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.logging.Logs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(name = Consts.FRAMEWORK_NAME + ".processor.log.enabled", havingValue = "true")
public class LogProcessor implements TestFinishEventProcessor {

    @Autowired
    TestFinishProperties.LoggingProperties properties;


    @Cacheable("logProperties")
    public LoggingPreferences getLoggingConfig() {
        LoggingPreferences logs = new LoggingPreferences();
        logs.enable(LogType.BROWSER, properties.getBrowser());
        logs.enable(LogType.CLIENT, properties.getClient());
        logs.enable(LogType.DRIVER, properties.getDriver());
        logs.enable(LogType.PERFORMANCE, properties.getPerformance());
        logs.enable(LogType.SERVER, properties.getServer());
        return logs;
    }

    //TODO validate
    protected String getDriverLogs(DriverHolder driverHolder) {
        Logs logs = driverHolder.getDriver().manage().logs();
        StringBuilder stringBuilder = new StringBuilder();
        logs.getAvailableLogTypes().forEach(type->logs.get(type).forEach(e->stringBuilder.append(e.toString())));
        return stringBuilder.toString();
    }

    /**
     * Grab all available log and add to finish event
     *
     * @param event
     * @param testExecutionContext
     */
    @Override
    public void processFinishEvent(TestFinishEvent event, TestExecutionContext testExecutionContext) {
        Map<String, String> logs = testExecutionContext.initedDriverHolders()
                .collect(Collectors.toMap(e -> e.getKey().getName(), e -> getDriverLogs(e.getValue())));
        event.setLogs(logs);
    }
}
