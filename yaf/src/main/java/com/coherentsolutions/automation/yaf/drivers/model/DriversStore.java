package com.coherentsolutions.automation.yaf.drivers.model;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.enums.Scope;
import com.coherentsolutions.automation.yaf.exception.DriverYafException;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
@Accessors(chain = true)
public class DriversStore {

    Map<EnvItem, DriverHolder> driverHolderMap;

    public DriversStore() {
        this.driverHolderMap = new ConcurrentHashMap<>();
    }

    public DriverHolder getRequiredDriver(EnvItem envItem) throws DriverYafException {
        DriverHolder holder = getDriver(envItem);
        if (holder == null) {
            throw new DriverYafException("Unable to get driver holder for " + envItem.getName());
        }
        return holder;
    }

    public DriverHolder getDriver(EnvItem envItem) {
        return driverHolderMap.get(envItem);
    }

    public DriversStore addDriver(EnvItem envItem, DriverHolder driverHolder) {
        driverHolderMap.put(envItem, driverHolder);
        return this;
    }

    public DriverHolder findInitedDriverByType(DeviceType deviceType) {
        return driverHolderMap.entrySet().stream().filter(e -> e.getKey().getType().equals(deviceType)).map(e -> e.getValue()).findFirst().orElse(null);
    }

    //TODO add driver stop event!

    public void clearAll() {
        driverHolderMap.keySet().forEach(key -> removeDriver(key));
    }

    public void clearAll(Scope scope) {
        driverHolderMap.entrySet().stream().filter(e -> e.getValue().getScope().equals(scope)).forEach(e -> removeDriver(e.getKey()));
    }

    public void clearAllBesides(Scope scope) {
        driverHolderMap.entrySet().stream().filter(e -> !e.getValue().getScope().equals(scope)).forEach(e -> removeDriver(e.getKey()));
    }

    public void removeDriver(EnvItem key) {
        DriverHolder holder = driverHolderMap.remove(key);
        if (holder != null) {
            holder.quit();
        }
    }

}
