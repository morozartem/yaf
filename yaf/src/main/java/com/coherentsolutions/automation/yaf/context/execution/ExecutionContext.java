package com.coherentsolutions.automation.yaf.context.execution;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.config.yaf.AppConfig;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.drivers.model.DriversStore;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.events.global.SuiteFinishEvent;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.Serializable;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Slf4j
public abstract class ExecutionContext implements Serializable {

    //list of all application properties (files, system properties, env), maven?, suite
    @Setter
    @Getter
    protected Properties properties;

    @Setter
    @Getter
    protected String testEnv;//TODO env?

    //TODO ? not autowire this bean, cause we need to create a human understandable message for user and provide some guidelines
    @Setter
    @Getter
    protected AppConfig yafConfig;

    @Getter
    protected DriversStore globalDriverStore;
    @Getter
    protected Map<String, DriversStore> suiteDriversStore;

    @Setter
    @Getter
    protected Map<String, Object> params;


    public ExecutionContext() {
        globalDriverStore = new DriversStore();
        suiteDriversStore = new ConcurrentHashMap<>();
        params = new ConcurrentHashMap<>();


    }

    public DriverHolder findInitedDriverHolderByType(String suiteName, DeviceType deviceType) {
        DriversStore suiteDS = suiteDriversStore.get(suiteName);
        DriverHolder holder = null;
        if (suiteDS != null) {
            holder = suiteDS.findInitedDriverByType(deviceType);
        }
        if (holder == null) {
            holder = globalDriverStore.findInitedDriverByType(deviceType);
        }
        return holder;
    }

    public abstract DriverHolder getDriverForSuite(String suiteName, EnvItem envItem);

    public abstract void addDriver(String suiteName, EnvItem envItem, DriverHolder driverHolder);

    @PreDestroy
    protected void clearContext() {
        log.debug("Clearing execution context");
        //we need to close all drivers
        globalDriverStore.clearAll();
        suiteDriversStore.values().forEach(ds -> ds.clearAll());
    }

    @EventListener
    @Order(1)
    public void afterSuite(SuiteFinishEvent event) {
        DriversStore suiteStore = this.suiteDriversStore.remove(event.getSuiteInfo().getSuiteName());
        if (suiteStore != null) {
            suiteStore.clearAll();
        }
    }
}
