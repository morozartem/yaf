package com.coherentsolutions.automation.yaf.listener;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.Env;
import com.coherentsolutions.automation.yaf.config.execution.ExecutionConfigInitializer;
import com.coherentsolutions.automation.yaf.context.execution.ExecutionContext;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.events.test.ClassFinishEvent;
import com.coherentsolutions.automation.yaf.events.test.RawTestFinishEvent;
import com.coherentsolutions.automation.yaf.events.test.TestFinishEvent;
import com.coherentsolutions.automation.yaf.events.test.TestStartEvent;
import com.coherentsolutions.automation.yaf.processor.finish.test.TestFinishEventProcessor;
import com.coherentsolutions.automation.yaf.utils.PropertiesUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class GlobalYafEventsListener extends YafListener {

    @Autowired
    PropertiesUtils propertiesUtils;

    @Autowired
    ExecutionContext executionContext;
    @Autowired(required = false)
    List<TestFinishEventProcessor> finishEventProcessorList;
    @Value("${processFinishEvents.onlyOnFail:true}")
    boolean onlyOnFail;

    @Autowired(required = false)
    Env stubEnv;

    @EventListener
    @Order(1)
    public void beforeTest(TestStartEvent event) {
        TestExecutionContext testExecutionContext = getTestExecutionContext();
        Env env;
        if (stubEnv == null) {
            String envConf = event.getTestInfo().getEnvSetup();
            env = ExecutionConfigInitializer.getInstance().getEnvConfig().getEnv(envConf);
        } else {
            log.debug("Using stub env bean!");
            env = stubEnv;
        }
        //we need to take this bean right from context cause it is threadlocal
        testExecutionContext.buildFromTestStartEvent(event, env);
    }

    @EventListener(RawTestFinishEvent.class)
    @Order(1)
    public void afterTest(RawTestFinishEvent event) {
        //gets raw test finish event, we need to append additional data
        log.info("After test {}", event.getTestInfo().getFullTestName());
        TestFinishEvent testFinishEvent = new TestFinishEvent();
        testFinishEvent.setTestInfo(event.getTestInfo());
        testFinishEvent.setTestResult(event.getTestResult());

        if ((onlyOnFail && !event.getTestResult().isSuccess()) || !onlyOnFail) {
            TestExecutionContext testExecutionContext = getTestExecutionContext();
            if (finishEventProcessorList != null) {
                finishEventProcessorList.forEach(p -> p.processFinishEvent(testFinishEvent, testExecutionContext));
            }
        }
        eventsService.sendEvent(testFinishEvent);
    }

    @EventListener
    @Order(1)
    public void afterClass(ClassFinishEvent event) {
        TestExecutionContext testExecutionContext = getTestExecutionContext();
        testExecutionContext.clearContext();
    }

}
