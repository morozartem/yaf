package com.coherentsolutions.automation.yaf.condition;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.enums.YafEnum;
import com.coherentsolutions.automation.yaf.utils.CapabilitiesUtils;
import org.openqa.selenium.Dimension;
import org.springframework.stereotype.Component;

@Component
public class BaseConditionMatcher implements ConditionMatcher {

    @Override
    public int matchScore(EnvItem envItem, YafCondition condition, Class pageClass, TestExecutionContext testExecutionContext) {
        int score = 0;
        score = getCompareEnumScore(envItem.getBrowser(), condition.browser())
                + getCompareEnumScore(envItem.getType(), condition.deviceType())
                + getCompareEnumScore(envItem.getMobileOS(), condition.mobileOs())
                + getCompareEnumScore(envItem.getOs(), condition.os())
                + getCompareScore(envItem.isSimulator(), condition.simulator())
                + matchScreenResolution(envItem, condition, testExecutionContext);
        return score;
    }

    //todo select proper place for that
    private int matchScreenResolution(EnvItem envItem, YafCondition condition, TestExecutionContext testExecutionContext) {
        //TODO !!! Check for mobile /desktop possible way to set resolution
        if (condition.width() == 0 && condition.height() == 0) {
            return 0;
        } else {
            //todo it is better to refactor this!!!!
            String resolution = envItem.getCapabilities().get(Consts.CAP_RESOLUTION);//screenResolution? screenwidth? window-size
            Dimension dimension = CapabilitiesUtils.getDimensionFromResolution(resolution);

            int score = 0;

            if (dimension == null) {
                return -1;
            }

            if (condition.width() != 0) {
                score += dimension.getWidth() <= condition.width() ? 1 : -1;
            }
            if (condition.height() != 0) {
                score += dimension.getHeight() <= condition.height() ? 1 : -1;
            }
            return score;
        }
    }

    protected int getCompareEnumScore(YafEnum actual, YafEnum expected) {
        if (actual == null || expected.toString().toLowerCase().equals("other")) {
            return 0;
        }
        return compareBoolean(actual.equals(expected));
    }

    protected int getCompareScore(int actual, int expected) {
        if (actual == expected) {
            return 1;
        } else {
            return -1;
        }
    }

    protected int getCompareScore(boolean actual, boolean expected) {
        if (actual == expected) {
            return 1;
        } else {
            return -1;
        }
    }

    protected int compareBoolean(boolean actual) {
        if (actual) {
            return 1;
        } else {
            return -1;
        }
    }
}
