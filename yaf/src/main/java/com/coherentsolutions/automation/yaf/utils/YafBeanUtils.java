package com.coherentsolutions.automation.yaf.utils;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */


import com.coherentsolutions.automation.yaf.bean.Yaf;
import com.coherentsolutions.automation.yaf.utils.store.CacheMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Service
public class YafBeanUtils {

    @Autowired
    @Qualifier("supportedClasses")
    protected List<Class> supportedClasses;

    @Autowired
    ApplicationContext applicationContext;

    CacheMap<Class, String> beanNames;

    public YafBeanUtils() {
        beanNames = new CacheMap<>((cls) -> {
            Configurable annotation = AnnotationUtils.getAnnotation(cls, Configurable.class);
            if (annotation != null) {
                return annotation.value();
            } else {
                return StringUtils.uncapitalize(cls.getSimpleName());//ClassUtils.getUserClass(cls).getName();
            }
        });
    }

    @Cacheable(value = "beanFields", key = "#cls")
    public List<Field> getClassYafFields(Class cls) {
        List<Field> fields = new ArrayList<>();
        ReflectionUtils.doWithFields(cls, field -> {
            field.setAccessible(true);
            fields.add(field);
        }, field -> (AnnotationUtils.findAnnotation(field.getType(), Yaf.class) != null)
                || (AnnotationUtils.findAnnotation(field, Yaf.class) != null)
                || isSupportedBean(field.getType()));
        return fields;
    }

    @Cacheable(value = "beanNames", key = "#name")
    public boolean containsBean(String name) {
        return applicationContext.containsBeanDefinition(name);
    }

    private boolean isSupportedBean(Class<?> objType) {
        return supportedClasses.stream().anyMatch(c -> c.isAssignableFrom(objType));
    }

    public <T extends Object> T getBean(Class cls) {
        //return (T) applicationContext.getBean(beanNames.get(cls));
        return (T) applicationContext.getBean(cls);
    }
}
