package com.coherentsolutions.automation.yaf.wait.driver.proxy;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */


import com.coherentsolutions.automation.yaf.wait.driver.BaseWait;
import com.coherentsolutions.automation.yaf.wait.driver.DriverWaitService;
import com.coherentsolutions.automation.yaf.wait.driver.WaitFor;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

@Service
public class WebElementProxy extends ProxyWithDriver {

    @Autowired
    DriverWaitService waitService;

    protected WaitFor getWaitAnnotation(Field field) {
        return AnnotationUtils.getAnnotation(field, WaitFor.class);
    }

    @Override
    public boolean canProxy(Field field) {
        return getWaitAnnotation(field) != null;
    }

    @Override
    protected Object proxyFieldWithDriver(Field field, Object fieldValue, Object obj, WebDriver driver) {
        WaitFor waitForAnnotation = getWaitAnnotation(field);

        // get waitFor child annotation
        Annotation waitAnnotation = Arrays.stream(field.getAnnotations())
                .filter(annotation -> annotation.annotationType().isAnnotationPresent(WaitFor.class))
                .findFirst().orElse(null);
        if (waitAnnotation == null) {
            //it basic wait for annotation
            waitAnnotation = waitForAnnotation;
        }

        ProxyFactory factory = new ProxyFactory(fieldValue);
        factory.addAdvice(new WebElementAdvice(waitForAnnotation, waitAnnotation, driver));
        return factory.getProxy();
    }

    public class WebElementAdvice implements MethodInterceptor {

        boolean alreadyLoaded;
        Annotation wait;
        WaitFor waitFor;
        WebDriver driver;

        public WebElementAdvice(WaitFor waitFor, Annotation wait, WebDriver driver) {
            this.wait = wait;
            this.waitFor = waitFor;
            this.driver = driver;
        }


        @Override
        public Object invoke(MethodInvocation methodInvocation) throws Throwable {
            if (!alreadyLoaded || waitFor.waitEveryTime()) {
                //wait for element according condition
                Object target = methodInvocation.getThis();
                List<WebElement> elements;
                if (target instanceof List) {
                    elements = (List<WebElement>) target;
                } else {
                    WebElement element = (WebElement) target;
                    elements = Arrays.asList(element);
                }
                BaseWait baseWait = (BaseWait) waitFor.waitClass().getConstructor(WaitFor.class, Annotation.class, List.class)
                        .newInstance(waitFor, wait, elements);
                waitService.waitFor(baseWait);
            }
            return methodInvocation.proceed();
        }

    }
}


