package com.coherentsolutions.automation.yaf.bean.factory.data;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.bean.factory.YafBeanFactory;
import com.coherentsolutions.automation.yaf.bean.factory.data.reader.DataReader;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.exception.GeneralYafException;
import com.coherentsolutions.automation.yaf.utils.YafBeanUtils;
import com.coherentsolutions.automation.yaf.utils.store.CacheMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(1)
@Slf4j
public class DataFactory implements YafBeanFactory {

    @Autowired
    YafBeanUtils beanUtils;
    CacheMap<Class, Boolean> processedClassesStore;

    public DataFactory() {
        processedClassesStore = new CacheMap<>((objType) -> objType.isAnnotationPresent(YafData.class));
    }

    @Override
    public boolean isSupportedBeanType(Class<?> objType) {
        return processedClassesStore.get(objType);
    }

    @Override
    public <T> T getBean(Class<?> objType, TestExecutionContext testExecutionContext) throws GeneralYafException {
        YafData data = AnnotationUtils.getAnnotation(objType, YafData.class);
        try {
            DataReader dataReader = beanUtils.getBean(data.reader());
            return (T) dataReader.readData(data, objType, testExecutionContext);
        } catch (Exception e) {
            throw new GeneralYafException(e.getMessage(), e);
        }
    }
}
