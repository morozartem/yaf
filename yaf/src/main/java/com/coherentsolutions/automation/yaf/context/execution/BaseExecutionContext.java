package com.coherentsolutions.automation.yaf.context.execution;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.config.yaf.YafConfig;
import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.drivers.model.DriversStore;
import com.coherentsolutions.automation.yaf.enums.Scope;
import com.coherentsolutions.automation.yaf.events.global.ExecutionStartEvent;
import com.coherentsolutions.automation.yaf.utils.EventsService;
import com.coherentsolutions.automation.yaf.utils.PropertiesUtils;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Data
@Accessors(chain = true)
@Component
@Slf4j
//@ConditionalOnMissingBean(ExecutionContext.class)
public class BaseExecutionContext extends ExecutionContext {


    public BaseExecutionContext(ApplicationContext applicationContext, PropertiesUtils propertiesUtils, EventsService eventsService) {
        //validate execution context
        yafConfig = null;
        try {
            //validate that we have at least one config if not - exit
            yafConfig = applicationContext.getBean(YafConfig.class);
        } catch (Exception ex) {
            log.error("You need to specify at least one bean that implements YafConfig!", ex);
            System.exit(1);
        }

        //populate initial context

        properties = propertiesUtils.getAllProperties();
        testEnv = PropertiesUtils.getPropertyValue(Consts.TESTENV, Consts.DEFAULT);

        // EnvConfig envConfig = ExecutionConfigInitializer.getInstance().getEnvConfig();

//        try {
//            context.setEnvironmentSettings(propertiesUtils.readAllEnvSettings((String) props.get(Consts.ENV_SETTINGS_FILE)));
//        } catch (GeneralYafException e) {
//            e.printStackTrace();//TODO!!
//        }

        //send custom event
        eventsService.sendEvent(new ExecutionStartEvent());

    }

    @Override
    public DriverHolder getDriverForSuite(String suiteName, EnvItem envItem) {
        DriversStore driversStore = suiteDriversStore.get(suiteName);
        DriverHolder driverHolder = null;
        if (driversStore != null) {
            driverHolder = driversStore.getDriver(envItem);
        }
        if (driverHolder == null) {
            driverHolder = globalDriverStore.getDriver(envItem);
        }
        return driverHolder;
    }

    @Override
    public void addDriver(String suiteName, EnvItem envItem, DriverHolder driverHolder) {
        if (driverHolder.getScope().equals(Scope.SUITE)) {
            DriversStore driversStore = suiteDriversStore.get(suiteName);
            if (driversStore == null) {
                driversStore = new DriversStore();
            }
            driversStore.addDriver(envItem, driverHolder);
            suiteDriversStore.put(suiteName, driversStore);
        } else {
            globalDriverStore.addDriver(envItem, driverHolder);
        }
    }
}
