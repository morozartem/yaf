package com.coherentsolutions.automation.yaf.config.execution;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.*;
import com.coherentsolutions.automation.yaf.enums.ParallelMode;
import com.coherentsolutions.automation.yaf.exception.GeneralYafException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.coherentsolutions.automation.yaf.consts.Consts.DEFAULT;
import static com.coherentsolutions.automation.yaf.utils.FileUtils.getResourceJsonFile;

//TODO write unit tests for that functionality
@Slf4j
public final class ExecutionConfigInitializer {

    public static String configsFolder = "envconfig";//todo props!
    private static ExecutionConfigInitializer instance;
    ObjectMapper mapper = new ObjectMapper();
    @Getter
    GeneralConfig generalConfig;
    EnvConfig envConfig;
    int minSessionsCount = 0;

    private ExecutionConfigInitializer() {
        //
        mapper.enable(JsonParser.Feature.ALLOW_COMMENTS);
    }

    public static ExecutionConfigInitializer getInstance() {
        if (instance == null) {
            synchronized (ExecutionConfigInitializer.class) {
                instance = new ExecutionConfigInitializer();
            }
        }
        return instance;
    }

    public EnvConfig getEnvConfig() {
        if (envConfig == null) {
            log.warn("Running tests on default config");
            envConfig = EnvConfig.defaultConfig();
        }
        return envConfig;
    }


    public void init(String fileName) throws GeneralYafException {
        try {
            if (fileName != null && !fileName.equals(DEFAULT)) {
                File general = getResourceJsonFile("general", configsFolder);
                generalConfig = mapper.readValue(general, GeneralConfig.class);

                File config = getResourceJsonFile(fileName, configsFolder);
                envConfig = mapper.readValue(config, EnvConfig.class);

                envConfig.setFarms(envConfig.getFarmsNames().stream().map(
                        f -> generalConfig.getFarms().stream().filter(ff -> ff.getName().equals(f)).findFirst().orElse(null)
                ).collect(Collectors.toList()));


                Map<String, Alias> aliasMap = generalConfig.getAliases().stream().collect(Collectors.toMap(a -> a.getName(), a -> a));

                envConfig.getConstEnvs().forEach(c -> {
                    c.processAlias(aliasMap);
                    if (minSessionsCount > c.getSessions()) {
                        minSessionsCount = c.getSessions();
                    }
                });

                envConfig.getEnvs().entrySet().forEach(e -> {
                    Env env = e.getValue();
                    env.forEach(envItem -> {
                        envItem.processAlias(aliasMap);
                        if (minSessionsCount > envItem.getSessions()) {
                            minSessionsCount = envItem.getSessions();
                        }
                        //process farm for env
                        if (!envConfig.getFarms().isEmpty()) {
                            Farm farm = null;
                            if (envItem.getFarmName() != null) {
                                farm = envConfig.getFarms().stream().filter(f -> f.getName().equalsIgnoreCase(envItem.getFarmName())).findFirst().orElse(null);
                            }
                            //find a farm for this type of env
                            List<Farm> possibleFarms = envConfig.getFarms().stream().filter(f -> f.getSupportedTypes().contains(envItem.getType())).collect(Collectors.toList());
                            if (possibleFarms.size() == 1) {
                                //only one farm is valid for this env
                                farm = possibleFarms.get(0);
                            } else if (possibleFarms.size() > 1) {
                                //get first farm that specialized only on env type
                                farm = possibleFarms.stream().filter(f -> f.getSupportedTypes().size() == 1).findFirst().orElse(null);
                            }
                            //we do not need to do any specific logic if there no farms found
                            envItem.setFarm(farm);
                        }
                    });
                    env.addAll(envConfig.getConstEnvs());
                });

            } else {
                //use default settings
                envConfig = EnvConfig.defaultConfig();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new GeneralYafException("Unable to load env config! " + ex.getMessage());
        }
    }

    public RunParallelSetup getRunSetUp() {
        //TODO !! add props to override this behavior
        RunParallelSetup setUp = new RunParallelSetup();
        if (envConfig != null && !DEFAULT.equals(envConfig.getName())) {

            int suiteThreads = envConfig.getThreads();
            if (suiteThreads == 0) {
                suiteThreads = envConfig.getEnvs().size();
                if (suiteThreads == 0) {
                    suiteThreads = 1;
                }
            }
            setUp.setSuiteThreadsCount(suiteThreads);

            //find proper thread count via priority of the place where it specified

            // general config property
            int threads = envConfig.getSessions();
            if (threads == 0) {
                //no global sessions param, lets set min sessions in all envs
                threads = minSessionsCount;
                if (threads == 0) {
                    // min value from all used farms
                    Farm farm = envConfig.getFarms().stream().min(Comparator.comparing(Farm::getSessions)).orElse(null);

                    if (farm != null) {
                        threads = farm.getSessions();
                    }
                    if (threads == 0) {
                        threads = 1;//set default value
                    }
                }
            }
            setUp.setThreadsCount(threads);
            if (threads == 1) {
                setUp.setParallelMode(ParallelMode.NONE);
            } else {
                setUp.setParallelMode(ParallelMode.CLASSES);
            }
        } else {
            setUp.setSuiteThreadsCount(1).setThreadsCount(1).setParallelMode(ParallelMode.NONE);
        }
        return setUp;
    }

}
