package com.coherentsolutions.automation.yaf.wait.driver;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.exception.DriverYafException;
import com.coherentsolutions.automation.yaf.utils.YafBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@Order()
@Slf4j
public class DriverWaitService {

    @Autowired
    YafBeanUtils beanUtils;

    @Autowired
    WaitProperties waitProperties;

    protected <R> R generalWait(BaseWait... waits) {
        if (waits.length > 1) {
            //todo may be add concurrency?
            for (BaseWait w : waits) {
                waitSingleWait(w);
            }
            return null;//todo what to return????
        } else {
            return waitSingleWait(waits[0]);
        }
    }

    public <R> R waitFor(BaseWait... waits) {
        try {
            return generalWait(waits);
        } catch (Exception ex) {
            BaseWait bw = Arrays.stream(waits).filter(w -> w.shouldThrowAssertError(ex)).findAny().orElse(null);
            if (bw != null) {
                throw new AssertionError(ex.getMessage());
            } else {
                throw ex;
            }
        }
    }

    public boolean waitForBool(BaseWait... waits) {
        try {
            generalWait(waits);
            return true;
        } catch (Exception ex) {
            log.debug("Skip wait exception " + ex.getMessage());
            return false;
        }
    }

    protected <R> R waitSingleWait(BaseWait wait) {
        WebDriverWait webDriverWait = buildWebDriverWait(wait);
        try {
            R r = wait.waitWithDriverWait(webDriverWait);
            return r;
        } catch (Exception ex) {
            if (wait.canRetry(ex)) {
                wait.getRetries().incrementAndGet();
                return waitSingleWait(wait);
            } else {
                throw ex;
            }
        }
    }

    protected WebDriverWait buildWebDriverWait(BaseWait wait) {
        WebDriver driver = wait.getDriver();
        if (driver == null) {
            TestExecutionContext testExecutionContext = beanUtils.getBean(TestExecutionContext.class);
            DeviceType deviceType = wait.getDeviceType();
            if (deviceType == null) {
                deviceType = DeviceType.WEB;
            }
            DriverHolder driverHolder = testExecutionContext.findInitedDriverHolderByType(deviceType);
            if (driverHolder != null) {
                driver = driverHolder.getDriver();
            }
            if (driver == null) {
                throw new DriverYafException("Could not get driver for wait! " + this);
            }
        }
        WaitConsts waitConsts = wait.getWaitConsts();
        if (waitConsts == null || waitConsts.equals(WaitConsts.EMPTY)) {
            return new WebDriverWait(driver, waitProperties.getExplicit().getTimeOut(), waitProperties.explicit.getInterval());
        } else {
            return new WebDriverWait(driver, waitConsts.getTimeOutInSeconds(), waitConsts.getPollIntervalInMillis());
        }
    }


}
