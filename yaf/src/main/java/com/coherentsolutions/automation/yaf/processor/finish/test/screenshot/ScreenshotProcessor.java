package com.coherentsolutions.automation.yaf.processor.finish.test.screenshot;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.events.test.TestFinishEvent;
import com.coherentsolutions.automation.yaf.processor.finish.test.TestFinishEventProcessor;
import com.coherentsolutions.automation.yaf.processor.finish.test.TestFinishProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Service
@Slf4j
@ConditionalOnProperty(name = "yaf.processor.screenshot.enabled", havingValue = "true")
public class ScreenshotProcessor implements TestFinishEventProcessor {

    @Autowired
    TestFinishProperties.ScreenshotProperties properties;

    @Override
    public void processFinishEvent(TestFinishEvent event, TestExecutionContext testExecutionContext) {
        Map<String, byte[]> screens = testExecutionContext.initedDriverHolders()
                .collect(Collectors.toMap(e -> e.getKey().getName(), e -> {
                    WebDriver driver = e.getValue().getDriver();
                    byte[] image = null;
                    if (e.getKey().getType().equals(DeviceType.WEB) && properties.isWebFullPage()) {
                        //todo test it
                        Screenshot screenshot = new AShot().takeScreenshot(driver);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        try {
                            ImageIO.write(screenshot.getImage(), "png", baos);
                            image = baos.toByteArray();
                        } catch (IOException ex) {
                            log.error(ex.getMessage(), ex);
                        }
                    } else {
                TakesScreenshot scrShot = ((TakesScreenshot) driver);
                        image = scrShot.getScreenshotAs(OutputType.BYTES);
            }
            if (properties.isCompress()) {
                //TODO compress
            }
            return image;
        }));
        event.setScreenshots(screens);

    }
}
