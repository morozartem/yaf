package com.coherentsolutions.automation.yaf.test;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.bean.factory.YafBeanProcessor;
import com.coherentsolutions.automation.yaf.bean.pom.IContextual;
import com.coherentsolutions.automation.yaf.bean.pom.Page;
import com.coherentsolutions.automation.yaf.context.execution.ExecutionContext;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.utils.EventsService;
import com.github.javafaker.Faker;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Slf4j
public abstract class BaseYafTest implements IContextual {

    @Autowired
    @Getter
    protected ApplicationContext applicationContext;

    @Autowired
    @Getter
    protected ExecutionContext executionContext;

    @Autowired
    @Getter
    @Setter
    protected TestExecutionContext testExecutionContext;

    @Autowired
    protected EventsService eventsService;

    @Autowired
    private YafBeanProcessor beanProcessor;

    @Autowired
    protected Faker faker;

    protected void initTestFields() {
        beanProcessor.processBean(this, testExecutionContext);
    }

    protected <T extends Page> T getPage(Class pageClass) {
        return getObject(pageClass);
    }

    protected <T> T getObject(Class pageClass) {
        return (T) beanProcessor.getBean(pageClass, testExecutionContext);
    }
}
