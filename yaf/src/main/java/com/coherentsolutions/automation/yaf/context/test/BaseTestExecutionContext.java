package com.coherentsolutions.automation.yaf.context.test;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.config.env.Env;
import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.events.driver.DriverStartEvent;
import com.coherentsolutions.automation.yaf.events.test.TestStartEvent;
import com.coherentsolutions.automation.yaf.exception.DriverYafException;
import com.coherentsolutions.automation.yaf.test.model.TestInfo;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Data
@Accessors(chain = true)
@Component
@org.springframework.context.annotation.Scope(Consts.SCOPE_THREADLOCAL)
@Slf4j
//@ConditionalOnMissingBean(TestExecutionContext.class)
public class BaseTestExecutionContext extends TestExecutionContext {

    protected DriverHolder findDriverHolderInContext(EnvItem envItem) {
        DriverHolder driverHolder = threadDrivesStore.getDriver(envItem);
        if (driverHolder == null) {
            driverHolder = executionContext.getDriverForSuite(suiteName, envItem);
        }
        return driverHolder;
    }

    protected void addDriverToContext(EnvItem envItem, DriverHolder driverHolder) {

        switch (driverHolder.getScope()) {
            case METHOD:
            case CLASS: {
                threadDrivesStore.addDriver(envItem, driverHolder);
                break;
            }
            case SUITE:
            case EXECUTION: {
                executionContext.addDriver(suiteName, envItem, driverHolder);
                break;
            }
        }
    }


    protected DriverHolder getDriverHolder(DeviceType deviceType, String... name) throws DriverYafException {
        log.info("Searching for " + deviceType + " with name " + name);
        EnvItem envItem = name.length != 0 ? env.findByName(name[0]) : env.findByType(deviceType);
        //search already initiated driver
        DriverHolder driverHolder = findDriverHolderInContext(envItem);
        if (driverHolder == null) {
            //no driver found, lets create the new driver
            log.info("Create new driver for " + deviceType + " with name " + name);
            driverHolder = driverManager.getDriver(envItem);
            if (driverHolder == null) {
                throw new DriverYafException("XASDAFAF");
            }
            addDriverToContext(envItem, driverHolder);
            //send new driver event
            applicationContext.publishEvent(new DriverStartEvent().setDriverHolder(driverHolder).setTestInfo(testInfo));
        }
        return driverHolder;
    }

    public DriverHolder getWebDriverHolder(String... name) throws DriverYafException {
        return getDriverHolder(DeviceType.WEB, name);
    }

    public DriverHolder getMobileDriverHolder(String... name) throws DriverYafException {
        return getDriverHolder(DeviceType.MOBILE, name);
    }

    public DriverHolder getDesktopDriverHolder(String... name) throws DriverYafException {
        return getDriverHolder(DeviceType.DESKTOP, name);
    }


    //todo refactor
    public void buildFromTestStartEvent(TestStartEvent event, Env env) {
        // clear test context, cause it could be populated from prev test,
        // cause it is thread local and our tests could be executed thread per class
        clearBeforeNewTest();
        this.env = env;
        TestInfo testInfo = event.getTestInfo();
        this.suiteName = testInfo.getSuiteInfo().getSuiteName();
        this.testName = testInfo.getTestName();
        this.startTime = event.getTimestamp();
        //this.currentTestState = TestState.STARTED;
        addStringParams(testInfo.getTestParams());
        this.testInfo = testInfo;

        if (testInfo.getTestMethodParams() != null) {
            Locale locale = (Locale) testInfo.getTestMethodParams().get("locale");
            if (locale != null) {
                this.locale = locale;
            }
        }

    }


}
