package com.coherentsolutions.automation.yaf.config.env;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.enums.Scope;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.util.HashMap;
import java.util.Map;

@Data
@Accessors(chain = true)
@ToString(callSuper = true)
public class EnvItem extends Alias {

    String alias;
    @JsonIgnore
    Farm farm;
    @JsonProperty("farm")
    String farmName;
    Scope scope = Scope.EXECUTION;
    boolean headless;//only for dev usage?

    public void processAlias(Map<String, Alias> aliasMap) {
        if (alias != null) {
            Alias aa = aliasMap.get(alias);
            if (aa != null) {
                // be attentive, we need to override props when copy bean
                int sessionsOverride = this.getSessions();
                String farmName = this.getFarmName();
                Scope scope = this.getScope();
                Map<String, String> capabilities = new HashMap<>(aa.getCapabilities());
                Map<String, String> appInfo = new HashMap<>(aa.getAppInfo());

                BeanUtils.copyProperties(aa, this);

                this.setSessions(sessionsOverride);
                this.setFarmName(farmName);
                this.setScope(scope);
                this.getCapabilities().putAll(capabilities);
                this.getAppInfo().putAll(appInfo);
            }
        }
    }

    public boolean isWebEnv() {
        return DeviceType.WEB.equals(type);
    }

    public boolean isMobileEnv() {
        return DeviceType.WEB.equals(type);
    }

    public boolean isDesktopEnv() {
        return DeviceType.WEB.equals(type);
    }

}
