package com.coherentsolutions.automation.yaf.utils.templates;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.exception.DataYafException;
import com.coherentsolutions.automation.yaf.utils.i18n.I18nService;
import com.github.javafaker.Faker;
import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mustache.MustacheResourceTemplateLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.Reader;
import java.lang.annotation.Annotation;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MustacheTemplateService implements TemplateService, ApplicationContextAware {

    @Autowired
    List<TemplateConfig> configs;

    @Autowired
    I18nService localizationService;

    @Autowired
    Faker faker;

    List<Mustache.TemplateLoader> loaders;

    Map<String, Object> constsCtx;

    @Setter
    ApplicationContext applicationContext;


    @PostConstruct
    protected void initLoaders() {
        loaders = configs.stream().map(c -> new MustacheResourceTemplateLoader(c.getPrefix(), c.getSuffix())).collect(Collectors.toList());
        constsCtx = new HashMap<>();

        Mustache.Lambda i18n = (frag, out) -> {
            String key = frag.execute();
            Locale l = Locale.getDefault();
            TestExecutionContext testExecutionContext = getTestExecutionContext();
            if (testExecutionContext != null) {
                l = testExecutionContext.getLocale();
            }
            out.write(localizationService.getMessage(key, l));
        };
        constsCtx.put("i18n", i18n);
        constsCtx.put("faker", faker);
    }


    protected LoaderMatchResult getTemplate(String name) {
        for (Mustache.TemplateLoader loader : loaders) {
            try {
                Reader reader = loader.getTemplate(name);
                return new LoaderMatchResult(loader, reader);
            } catch (Exception e) {
                //
                System.out.println("Try " + loader + " ex " + e.getMessage());
            }
        }
        return null;//TODO may be rethrow last exception?
    }

    public String processTemplateString(String templateString, Map<String, Object> ctx) throws DataYafException {
        Template tmpl = Mustache.compiler().compile(templateString);
        return execTemplate(tmpl, ctx);
    }

    public String processTemplate(String templateName, Map<String, Object> ctx) throws DataYafException {
        LoaderMatchResult loaderMatchResult = getTemplate(templateName);
        if (loaderMatchResult == null) {
            throw new DataYafException("Unable to find template " + templateName);
        }
        //TODO cache?
        Template tmpl = Mustache.compiler()
                .emptyStringIsFalse(true).defaultValue("")
                .withLoader(loaderMatchResult.getLoader())
                .compile(loaderMatchResult.getReader());

        return execTemplate(tmpl, ctx);
    }

    private String execTemplate(Template template, Map<String, Object> ctx) {
        ctx.putAll(constsCtx);
        TestExecutionContext testExecutionContext = getTestExecutionContext();
        if (testExecutionContext != null) {

            List<Annotation> annotations = testExecutionContext.getTestInfo().getAnnotationList();

            List<TemplateValue> templateValues = annotations
                    .stream().filter(a -> a.annotationType().equals(TemplateValue.class)).map(a -> (TemplateValue) a).collect(Collectors.toList());
            TemplateValues tValues = (TemplateValues) annotations.stream().filter(a -> a.annotationType().equals(TemplateValues.class)).findFirst().orElse(null);
            if (tValues != null) {
                templateValues.addAll(Arrays.asList(tValues.value()));
            }
            templateValues.forEach(a -> ctx.put(a.key(), a.value()));

            ctx.put("tctx", getTestExecutionContext());
        }
        return template.execute(ctx);
    }

    public TestExecutionContext getTestExecutionContext() {
        return applicationContext.getBean(TestExecutionContext.class);
    }


    @Data
    @AllArgsConstructor
    protected class LoaderMatchResult {
        Mustache.TemplateLoader loader;
        Reader reader;
    }

}
