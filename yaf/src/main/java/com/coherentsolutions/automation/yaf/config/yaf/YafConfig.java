package com.coherentsolutions.automation.yaf.config.yaf;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.bean.pom.Page;
import com.coherentsolutions.automation.yaf.bean.pom.factory.DefaultYafLocatorFactory;
import com.coherentsolutions.automation.yaf.bean.pom.factory.YafLocatorFactory;
import com.coherentsolutions.automation.yaf.bean.utils.Utils;
import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.utils.templates.TemplateConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.context.support.SimpleThreadScope;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component("executionConfig")
@SpringBootConfiguration
@ComponentScan(basePackages = {"com.coherentsolutions"})
@EnableCaching
@EnableAsync
@EnableAutoConfiguration
@Slf4j
public abstract class YafConfig implements AppConfig {

    protected ApplicationArguments arguments;
    @Value("${spring.messages.basename:messages}")
    String messagesPath;

    @Bean
    public static CustomScopeConfigurer testScopeConfigurer() {
        CustomScopeConfigurer customScopeConfigurer = new CustomScopeConfigurer();
        customScopeConfigurer.addScope(Consts.SCOPE_THREADLOCAL, new SimpleThreadScope());
        return customScopeConfigurer;
    }

    public YafConfig(ApplicationArguments args) {
        log.info("Building general execution config");
        arguments = args;
    }

    @Bean
    public TaskExecutor taskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }

    @Bean
    public Faker faker() {
        return new Faker(Locale.getDefault());
    }

    @Bean(name = "supportedClasses")
    public List<Class> supportedClasses() {
        List<Class> classes = new ArrayList<>();
        classes.add(Page.class);
        classes.add(Utils.class);
        return classes;
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    @Component
    @ConfigurationPropertiesBinding
    public static class DeviceTypeConverter implements Converter<String, DeviceType> {

        @Override
        public DeviceType convert(String from) {
            return DeviceType.valueOfName(from.toLowerCase());

        }
    }

    @Bean
    @ConditionalOnMissingBean(YafLocatorFactory.class)
    public YafLocatorFactory locatorFactory() {
        return new DefaultYafLocatorFactory();
    }

    //todo add other caches
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        List<Cache> caches = new ArrayList<>();
        caches.add(new ConcurrentMapCache("beanFields"));
        caches.add(new ConcurrentMapCache("beanNames"));
        cacheManager.setCaches(caches);
        return cacheManager;
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {

        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames(messagesPath);
        source.setDefaultLocale(Locale.US);
        source.setUseCodeAsDefaultMessage(true);
        source.setFallbackToSystemLocale(true);

        return source;
    }

    @Bean
    public Locale locale() {
        return Locale.getDefault();
    }

    @Bean
    public TemplateConfig dataConfJson() {
        return new TemplateConfig("/", ".json");
    }

    @Bean
    public TemplateConfig dataConfXml() {
        return new TemplateConfig("/", ".xml");
    }

}
