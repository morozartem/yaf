package com.coherentsolutions.automation.yaf.bean.factory.data.reader;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.bean.factory.data.YafData;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.exception.GeneralYafException;
import com.coherentsolutions.automation.yaf.test.YafTest;
import com.coherentsolutions.automation.yaf.test.model.TestInfo;
import com.coherentsolutions.automation.yaf.utils.templates.TemplateService;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.file.Path;
import java.nio.file.Paths;

@Data
@Accessors(chain = true)
public abstract class BaseFileReader implements DataReader {

    @Autowired
    DataFactoryProperties properties;

    @Autowired
    TemplateService templateService;

    Path projectPath = Paths.get(System.getProperty("user.dir"));

    protected String getFileExt() {
        return "." + properties.getFileExt();
    }

    protected String getFileData(YafData yafData, TestExecutionContext testExecutionContext) throws GeneralYafException {
        String fileName = null;

        if (!yafData.fileName().isEmpty()) {
            fileName = yafData.fileName();
        } else {
            TestInfo testInfo = testExecutionContext.getTestInfo();
            YafTest yafTest = testInfo.getYafTest();
            if (yafTest != null) {
                fileName = yafTest.dataFile();
                if (fileName == null) {
                    fileName = yafTest.name();
                }
            }
            if (fileName == null) {
                fileName = testInfo.getTestMethodName();
            }
        }
        if (fileName == null) {
            throw new GeneralYafException("Unable to load data file for test " + testExecutionContext.getTestInfo().getFullTestName());
        } else {
            // File file = FileUtils.getResourceFile(fileName, getFileExt(), properties.getDataFolder(), true, true);
            return templateService.processTemplate(properties.getDataFolder() + "/" + fileName);
            //return null;
        }
    }

}
