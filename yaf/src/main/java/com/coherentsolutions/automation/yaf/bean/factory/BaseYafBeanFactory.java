package com.coherentsolutions.automation.yaf.bean.factory;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.bean.pom.IContextual;
import com.coherentsolutions.automation.yaf.condition.ConditionMatchService;
import com.coherentsolutions.automation.yaf.config.env.Env;
import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.exception.EnvSetupYafException;
import com.coherentsolutions.automation.yaf.exception.GeneralYafException;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Component
@Order()
@Slf4j
public class BaseYafBeanFactory implements YafBeanFactory {

    @Autowired
    @Getter
    protected ApplicationContext applicationContext;

    @Autowired
    ConditionMatchService matchService;

    @Override
    public boolean isSupportedBeanType(Class<?> objType) {
        return true;//may be check if it is bean
    }

    @Override
    public <T> T getBean(Class<?> objType, TestExecutionContext testExecutionContext) throws GeneralYafException {
        Map<String, ?> beansOfType = applicationContext.getBeansOfType(objType);
        T bean = null;
        if (beansOfType.size() == 1) {
            //only one possible impl
            bean = (T) beansOfType.get(beansOfType.keySet().stream().findFirst().get());
        } else {
            //find proper bean according env
            Env env = testExecutionContext.getEnv();

            bean = (T) beansOfType.values().parallelStream()
                    .collect(Collectors.toMap(p -> p, p -> matchService.matches(p.getClass(), env, testExecutionContext)))
                    .entrySet().parallelStream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                    .filter(e -> e.getValue() >= 0)
                    .map(e -> e.getKey())
                    .findFirst()
                    .orElseThrow(() -> new EnvSetupYafException("Unable to init " + objType.getCanonicalName() + " due mulpitle variants!"));
        }
        if (bean != null) {
            if (bean instanceof IContextual) {
                ((IContextual) bean).setTestExecutionContext(testExecutionContext);
            }
        }
        return bean;
    }

}
