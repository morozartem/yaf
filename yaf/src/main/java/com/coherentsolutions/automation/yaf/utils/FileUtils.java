package com.coherentsolutions.automation.yaf.utils;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.exception.GeneralYafException;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

public class FileUtils {

    public static String normalizeFileName(String name, String ext) {
        if (!ext.startsWith(".")) {
            ext = "." + ext;
        }
        if (!name.endsWith(ext)) {
            name += ext;
        }
        return name;
    }

    public static File getResourceJsonFile(String name, String resourceFolder) throws GeneralYafException {
        return getResourceFile(name, Consts.JSON, resourceFolder, true, true);
    }

    public static File getResourceFile(String name, String ext, String resourceFolder, boolean normalize, boolean tryToLoadExternal) throws GeneralYafException {
        if (normalize) {
            name = FileUtils.normalizeFileName(name, ext);
        }
        String classpathPath = "classpath:" + (resourceFolder != null ? resourceFolder + "/" : "") + name;
        try {
            File file = ResourceUtils.getFile(classpathPath);
            return file;
        } catch (FileNotFoundException e) {
            if (tryToLoadExternal) {
                try {
                    File file = ResourceUtils.getFile("." + File.separator + name);
                    return file;
                } catch (FileNotFoundException fileNotFoundException) {
                    // no need
                }
            }
            throw new GeneralYafException("File " + name + " not found!");

        }

    }
}
