package com.coherentsolutions.automation.yaf.wait.driver;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.enums.DeviceType;
import lombok.Data;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public abstract class BaseWait {

    DeviceType deviceType;
    WebDriver driver;
    List<By> locators;
    List<WebElement> elements;
    WaitConsts waitConsts;
    protected Annotation waitAnnotation;
    protected WaitFor waitFor;

    protected List<Class> assertExceptionsList;
    protected List<Class> retryExceptionList;
    protected int retryTimes;
    protected AtomicInteger retries = new AtomicInteger(0);

    public BaseWait(WaitFor waitFor, Annotation waitAnnotation, List<WebElement> elements) {
        this.elements = elements;
        this.waitAnnotation = waitAnnotation;
        this.waitFor = waitFor;
        this.waitConsts = waitFor.waitConsts();
        this.assertExceptionsList = Arrays.asList(waitFor.assertExceptionsList());
        this.retryExceptionList = Arrays.asList(waitFor.retryExceptionList());
        this.retryTimes = waitFor.retryTimes();
        processCustomWaitAnnotation();
    }

    public BaseWait(WebElement element) {
        this.elements = Arrays.asList(element);
    }

    public BaseWait(By by) {
        this.locators = Arrays.asList(by);
    }

    public static void main(String[] args) {
        Class[] ss = {};
        List<Class> dd = Arrays.asList(ss);
        System.out.println(dd);
    }

    public BaseWait withDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    public BaseWait withDriver(WebDriver driver) {
        this.driver = driver;
        return this;
    }

    public BaseWait withLocators(List<By> locators) {
        this.locators = locators;
        return this;
    }

    public BaseWait withLocator(By locator) {
        if (this.locators == null) {
            this.locators = new ArrayList<>();
        }
        this.locators.add(locator);
        return this;
    }

    public BaseWait withElements(List<WebElement> elements) {
        this.elements = elements;
        return this;
    }

    public BaseWait withElement(WebElement element) {
        if (this.elements == null) {
            this.elements = new ArrayList<>();
        }
        this.elements.add(element);
        return this;
    }

    public BaseWait withWaitConsts(WaitConsts waitConsts) {
        this.waitConsts = waitConsts;
        return this;
    }

    public boolean canRetry(Exception ex) {
        return (retryExceptionList != null && retries.get() < retryTimes && retryExceptionList.contains(ex.getClass()));
    }

    public boolean shouldThrowAssertError(Exception ex) {
        return (assertExceptionsList != null && assertExceptionsList.contains(ex.getClass()));
    }

    //Add more assertExceptionsList or retries if you want to make them dynamic
    protected abstract void processCustomWaitAnnotation();

    <R> R waitWithDriverWait(WebDriverWait webDriverWait) {
        return wait(webDriverWait);
    }

    protected abstract <R> R wait(WebDriverWait wait);

}
