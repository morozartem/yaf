package com.coherentsolutions.automation.yaf.config.env;

/*-
 * #%L
 * yaf
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.enums.Browser;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.enums.Scope;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.coherentsolutions.automation.yaf.consts.Consts.DEFAULT;

@Data
@Accessors(chain = true)
@ToString(callSuper = true)
public class EnvConfig extends Nameable {

    @JsonProperty("farms")
    List<String> farmsNames;
    @JsonIgnore
    List<Farm> farms;
    int threads;
    int sessions;
    List<EnvItem> constEnvs;
    Map<String, Env> envs;

    public EnvConfig() {
        farmsNames = new ArrayList<>();
        constEnvs = new ArrayList<>();
        farms = new ArrayList<>();
        envs = new HashMap<>();
    }

    //TODO refactor!!!
    public static EnvConfig defaultConfig() {
        //read farm??
        EnvConfig def = new EnvConfig();
        def.setSessions(1);
        Env defEnv = new Env();

        EnvItem envItem = new EnvItem();
        envItem.setBrowser(Browser.CHROME);
        envItem.setType(DeviceType.WEB);
        envItem.setScope(Scope.EXECUTION);
        envItem.setName(DEFAULT);

        defEnv.add(envItem);
        Map<String, Env> envs = new HashMap<>();
        envs.put(null, defEnv);
        def.setEnvs(envs);
        return def;
    }

    public Env getEnv(String name) {
        return envs.get(name);
    }

}
