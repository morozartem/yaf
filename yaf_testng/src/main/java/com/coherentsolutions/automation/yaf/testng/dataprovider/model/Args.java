package com.coherentsolutions.automation.yaf.testng.dataprovider.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.assertj.core.util.Arrays;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class Args {

    List<Object> args;

    public void addArg(Object arg){
        if(args==null){
            args = new ArrayList<>();
        }
        args.add(arg);
    }

    public void addAllArgs(List<Object> args){
        args.forEach(a->addArg(a));
    }

    public int length(){
        return args==null?0:args.size();
    }

    public static Args build(Object... objects){
        return new Args(Arrays.asList(objects));
    }

    public Object[] toArray(){
        return args.toArray();
    }
}
