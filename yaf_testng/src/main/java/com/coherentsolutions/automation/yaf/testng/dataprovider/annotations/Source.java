package com.coherentsolutions.automation.yaf.testng.dataprovider.annotations;

import com.coherentsolutions.automation.yaf.testng.dataprovider.DataProcessor;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Inherited
@Documented
public @interface Source {

    Class<? extends DataProcessor> processor();
}
