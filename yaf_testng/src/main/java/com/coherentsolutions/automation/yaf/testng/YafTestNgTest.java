package com.coherentsolutions.automation.yaf.testng;

import com.coherentsolutions.automation.yaf.events.global.SuiteStartEvent;
import com.coherentsolutions.automation.yaf.events.test.ClassFinishEvent;
import com.coherentsolutions.automation.yaf.events.test.ClassStartEvent;
import com.coherentsolutions.automation.yaf.events.test.RawTestFinishEvent;
import com.coherentsolutions.automation.yaf.events.test.TestStartEvent;
import com.coherentsolutions.automation.yaf.exception.DataYafException;
import com.coherentsolutions.automation.yaf.test.model.ClassInfo;
import com.coherentsolutions.automation.yaf.test.model.SuiteInfo;
import com.coherentsolutions.automation.yaf.test.model.TestInfo;
import com.coherentsolutions.automation.yaf.testng.dataprovider.YafDataProvider;
import com.coherentsolutions.automation.yaf.testng.utils.TestNgUtils;
import lombok.extern.slf4j.Slf4j;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;

@Slf4j
public abstract class YafTestNgTest extends BaseTestNgTest {


    //custom dataProvider
    public static final String DATA = "data";

    @DataProvider
    public static Object[][] data(Method m, ITestContext iTestContext) throws DataYafException {
        return YafDataProvider.data(m, iTestContext);
    }

    @BeforeSuite
    public void beforeSuite(ITestContext testContext) {
        SuiteStartEvent suiteStartEvent = new SuiteStartEvent();
        SuiteInfo suiteInfo = TestNgUtils.buildSuiteInfo(testContext);
        suiteStartEvent.setSuiteInfo(suiteInfo);
        eventsService.sendEvent(suiteStartEvent);
        log.info("Starting suite {}", suiteInfo.getSuiteName());
    }

    @BeforeClass
    public void beforeClass(ITestContext testContext) {
        ClassStartEvent classStartEvent = new ClassStartEvent();
        ClassInfo classInfo = TestNgUtils.buildClassInfo(testContext);
        classStartEvent.setClassInfo(classInfo);
        eventsService.sendEvent(classStartEvent);
        log.info("Starting class {}", classInfo.getClassName());
    }

    @AfterClass
    public void afterClass(ITestContext testContext) {
        ClassFinishEvent classStopEvent = new ClassFinishEvent();
        ClassInfo classInfo = TestNgUtils.buildClassInfo(testContext);
        classStopEvent.setClassInfo(classInfo);
        eventsService.sendEvent(classStopEvent);
        log.info("Stopping class {}", classInfo.getClassName());
    }

    @BeforeMethod
    public void beforeMethod(ITestContext testContext, Method method, Object[] methodArgs) {
        TestStartEvent testStartEvent = new TestStartEvent();

        TestInfo testInfo = TestNgUtils.buildTestInfo(testContext, method, methodArgs);
        testStartEvent.setTestInfo(testInfo);

        eventsService.sendEvent(testStartEvent);
        log.info("Starting test {}", testInfo.getFullTestName());

    }

    @BeforeMethod(dependsOnMethods = "beforeMethod")
    public void initTestClassObjects(ITestContext testContext) {
        initTestFields();
        log.info("Pages inited for test {}", testContext.getName());
    }

    @AfterMethod
    public void afterTestMethod(ITestContext testContext, Method method, Object[] methodArgs, ITestResult testResult) {
        RawTestFinishEvent testFinishEvent = new RawTestFinishEvent();
        testFinishEvent.setTestInfo(testExecutionContext.getTestInfo());
        testFinishEvent.setTestResult(TestNgUtils.buildTestResult(testResult));

        eventsService.sendEvent(testFinishEvent);
    }

}
