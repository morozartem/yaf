package com.coherentsolutions.automation.yaf.testng.dataprovider;

import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.exception.DataYafException;
import com.coherentsolutions.automation.yaf.testng.dataprovider.model.Args;

import java.lang.annotation.Annotation;
import java.util.List;

public abstract class DataProcessor<T extends Args> {

    protected abstract Class getSupportedAnnotationClass();

    protected abstract List<T> process(Annotation annotation, TestExecutionContext testExecutionContext);

    public List<T> processData(Annotation annotation, TestExecutionContext testExecutionContext) throws DataYafException {
        if (annotation != null && annotation.annotationType().equals(getSupportedAnnotationClass())) {
            return process(annotation, testExecutionContext);
        } else {
            throw new DataYafException("Could not find proper processor class for annotation "+ annotation);
        }

    }
}
