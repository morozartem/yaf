package com.coherentsolutions.automation.yaf.testng.utils;

import com.coherentsolutions.automation.yaf.enums.TestResult;
import com.coherentsolutions.automation.yaf.enums.TestState;
import com.coherentsolutions.automation.yaf.exception.GeneralYafException;
import com.coherentsolutions.automation.yaf.test.YafTest;
import com.coherentsolutions.automation.yaf.test.model.ClassInfo;
import com.coherentsolutions.automation.yaf.test.model.SuiteInfo;
import com.coherentsolutions.automation.yaf.test.model.TestInfo;
import com.coherentsolutions.automation.yaf.testng.YafTransformer;
import org.springframework.core.annotation.AnnotationUtils;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import static com.coherentsolutions.automation.yaf.consts.Consts.ENV_SETTING_PARAM;

public class TestNgUtils {

    public static SuiteInfo buildSuiteInfo(ITestContext testContext) {
        SuiteInfo suiteInfo = new SuiteInfo();
        ISuite suite = testContext.getSuite();
        suiteInfo.setSuiteName(suite.getName());
        suiteInfo.setSuiteParams(suite.getXmlSuite().getAllParameters());
        return suiteInfo;
    }

    public static ClassInfo buildClassInfo(ITestContext testContext) {
        ClassInfo classInfo = new ClassInfo();

        return classInfo;//TODO

    }

    public static TestInfo buildTestInfo(ITestContext testContext, Method method, Object[] methodArgs) {
        TestInfo testInfo = new TestInfo();
        testInfo.setTestId(UUID.randomUUID().toString());
        testInfo
                .setTestName(testContext.getName())
                .setTestMethod(method);

        testInfo.setTestClass(method.getDeclaringClass());

        testInfo.setTestParams(testContext.getCurrentXmlTest().getLocalParameters());

        testInfo.setYafTest(AnnotationUtils.getAnnotation(method, YafTest.class));

        if (methodArgs != null && methodArgs.length > 0) {
            //todo move to global utils
            Map<String, Object> testMethodParams = new HashMap<>();
            List<String> methodParamsNames = Arrays.stream(method.getParameters()).map(p -> p.getName()).collect(Collectors.toList());
            for (int i = 0; i < methodParamsNames.size(); i++) {
                String paramName = methodParamsNames.get(i);
                Object paramValue = methodArgs[i];
                testMethodParams.put(paramName, paramValue);
            }
            testInfo.setTestMethodParams(testMethodParams);
        }
        testInfo.setRunnerContext(testContext);

        testInfo.setSuiteInfo(buildSuiteInfo(testContext));

        testInfo.setEnvSetup(testInfo.getSuiteInfo().getSuiteParams().get(ENV_SETTING_PARAM));
        return testInfo;
    }

    public static TestResult buildTestResult(ITestResult iTestResult) {
        TestResult testResult = new TestResult();
        testResult.setSuccess(iTestResult.isSuccess());
        testResult.setExecutionTime(iTestResult.getEndMillis() - iTestResult.getStartMillis());
        testResult.setError(iTestResult.getThrowable());
        switch (iTestResult.getStatus()) {
            case 1: {
                testResult.setState(TestState.SUCCESS);
                break;
            }
            case 2: {
                testResult.setState(TestState.FAIL);
                break;
            }
            case 3: {
                testResult.setState(TestState.SKIP);
                break;
            }
            default: {
                testResult.setState(TestState.UNKNOWN);
                break;
            }
        }
        return testResult;

    }

    private static TestNG getTestNgInstance() throws GeneralYafException {
        try {
            Field field = TestNG.class.getDeclaredField("m_instance");
            field.setAccessible(true);
            TestNG testNG = (TestNG) field.get(null);
            return testNG;
        } catch (Exception e) {
            throw new GeneralYafException("Unable to get TestNG instance ! " + e.getMessage());
        }
    }


    public static void addYafListener() throws GeneralYafException {
        try {
            TestNG testNG = getTestNgInstance();
            testNG.addListener(new YafTransformer());
        } catch (Exception e) {
            throw new GeneralYafException("Unable to patch TestNG listeners! " + e.getMessage());
        }
    }

    public static void setSuiteThreadCount(int threadCount) throws GeneralYafException {
        try {
            TestNG testNG = getTestNgInstance();
            Field suiteThreads = TestNG.class.getDeclaredField("m_suiteThreadPoolSize");
            suiteThreads.setAccessible(true);
            suiteThreads.set(testNG, threadCount);
        } catch (Exception e) {
            throw new GeneralYafException("Unable to patch TestNG suite thread count! " + e.getMessage());
        }
    }

    public static XmlSuite cloneAndModifySuite(XmlSuite initialSuite, String execConfig) {
        XmlSuite newSuite = new XmlSuite();
        newSuite.setName(initialSuite.getName() + "__" + execConfig);

        Map<String, String> newSuiteParams = new HashMap<>(initialSuite.getAllParameters());
        newSuiteParams.put(ENV_SETTING_PARAM, execConfig);
        newSuite.setParameters(newSuiteParams);

        newSuite.setListeners(initialSuite.getListeners());
        newSuite.setParallel(initialSuite.getParallel());
        newSuite.setThreadCount(initialSuite.getThreadCount());

        //iterate thru tests
        List<XmlTest> newTests = initialSuite.getTests().stream().map(t -> {
            System.out.println("----- " + t.getLocalParameters());
            XmlTest test = new XmlTest();
            test.setName(t.getName() + "--" + execConfig);

            Map<String, String> newTestParams = new HashMap<>(t.getLocalParameters());
            //newTestParams.putAll(newSuiteParams);
            //newTestParams.put(ENV_SETTING_PARAM, execConfig);

            test.setParameters(newTestParams);

            List<XmlClass> newClasses = t.getClasses().stream().map(c -> {
                XmlClass cc = new XmlClass(c.getName());
                cc.setXmlTest(test);
                return cc;
            }).collect(Collectors.toList());

            test.setClasses(newClasses);

            List<XmlPackage> packages = t.getPackages().stream().map(p -> {
                XmlPackage pp = new XmlPackage(p.getName());

                return pp;
            }).collect(Collectors.toList());
            test.setPackages(packages);

            test.setXmlSuite(newSuite);

            System.out.println("--444--- " + test.getLocalParameters());

            return test;
        }).collect(Collectors.toList());

        newSuite.setTests(newTests);

        System.out.println(newSuite.toXml());

        return newSuite;
    }
}
