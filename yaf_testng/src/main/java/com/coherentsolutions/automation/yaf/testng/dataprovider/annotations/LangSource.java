package com.coherentsolutions.automation.yaf.testng.dataprovider.annotations;

import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.testng.dataprovider.DataProcessor;
import com.coherentsolutions.automation.yaf.testng.dataprovider.model.Args;
import com.coherentsolutions.automation.yaf.utils.i18n.Lang;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.annotation.ElementType.METHOD;

@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({METHOD})
@Source(processor = LangSource.LocaleProcessor.class)
public @interface LangSource {

    Lang[] value();


    @Component
    class LocaleProcessor extends DataProcessor<Args> {

        @Override
        public Class getSupportedAnnotationClass() {
            return LangSource.class;
        }

        @Override
        public List<Args> process(Annotation annotation, TestExecutionContext testExecutionContext) {
            LangSource source = (LangSource) annotation;
            return Arrays.stream(source.value()).map(l -> Args.build(l.getLocale())).collect(Collectors.toList());
        }
    }

}
