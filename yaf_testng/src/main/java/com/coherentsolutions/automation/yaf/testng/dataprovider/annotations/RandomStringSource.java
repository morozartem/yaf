package com.coherentsolutions.automation.yaf.testng.dataprovider.annotations;

import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.testng.dataprovider.DataProcessor;
import com.coherentsolutions.automation.yaf.testng.dataprovider.model.Args;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.annotation.ElementType.METHOD;

@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({METHOD})
@Source(processor = RandomStringSource.RandomStringProcessor.class)
public @interface RandomStringSource {

    int size() default 1;


    @Component
    class RandomStringProcessor extends DataProcessor<Args> {

        @Autowired
        Faker faker;

        @Override
        public Class getSupportedAnnotationClass() {
            return RandomStringSource.class;
        }

        @Override
        public List<Args> process(Annotation annotation, TestExecutionContext testExecutionContext) {
            RandomStringSource source = (RandomStringSource) annotation;
            return faker.lorem().words(source.size()).stream()
                    .map(i -> Args.build(i))
                    .collect(Collectors.toList());
        }
    }

}
