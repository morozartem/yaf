package com.coherentsolutions.automation.yaf.testng.dataprovider.annotations;

import com.coherentsolutions.automation.yaf.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.testng.dataprovider.DataProcessor;
import com.coherentsolutions.automation.yaf.testng.dataprovider.model.Args;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static java.lang.annotation.ElementType.METHOD;

@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({METHOD})
@Source(processor = RandomIntSource.Processor.class)
public @interface RandomIntSource {

    int size() default 1;

    int min() default Integer.MIN_VALUE;

    int max() default Integer.MAX_VALUE;

    boolean onlyPositive() default true;

    boolean commonRange() default true;

    @Component
    class Processor extends DataProcessor<Args> {

        @Override
        public Class getSupportedAnnotationClass() {
            return RandomIntSource.class;
        }

        @Override
        public List<Args> process(Annotation annotation, TestExecutionContext testExecutionContext) {
            RandomIntSource source = (RandomIntSource) annotation;
            int min = source.commonRange() ? -100 : source.min();
            int max = source.commonRange()?100:source.max();
            min = source.onlyPositive()?0:min;
            return new Random().ints(source.size(), min, max).boxed()
                    .map(i->Args.build(i))
                    .collect(Collectors.toList());
        }
    }

}
