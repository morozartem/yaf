package com.coherentsolutions.automation.yaf.yaf_allure;

import com.coherentsolutions.automation.yaf.config.yaf.ModuleProperties;
import com.coherentsolutions.automation.yaf.consts.Consts;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = Consts.FRAMEWORK_NAME + ".allure")
public class AllureProperties extends ModuleProperties {

    boolean fullLogAllTests;
}
