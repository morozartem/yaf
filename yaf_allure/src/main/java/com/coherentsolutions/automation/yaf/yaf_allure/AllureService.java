package com.coherentsolutions.automation.yaf.yaf_allure;

/*-
 * #%L
 * yaf_allure
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.events.test.TestFinishEvent;
import com.coherentsolutions.automation.yaf.events.test.TestStartEvent;
import com.coherentsolutions.automation.yaf.test.YafTest;
import com.coherentsolutions.automation.yaf.test.model.TestInfo;
import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

import static io.qameta.allure.util.ResultsUtils.*;

@Service
@Slf4j
@ConditionalOnProperty(name = Consts.FRAMEWORK_NAME + ".allure.enabled", havingValue = "true")
public class AllureService {

    AllureLifecycle lifecycle;

    Charset charset = Charset.forName("UTF-8");

    @Autowired
    AllureProperties allureProperties;

    public static final String UID = "uuid";

    public AllureService() {
        lifecycle = Allure.getLifecycle();

    }

    public void testStarted(final TestInfo testInfo) {
        String uuid = UUID.randomUUID().toString();
        testInfo.getTestParams().put(UID, uuid);
        io.qameta.allure.model.TestResult result = createTestResult(uuid, testInfo);
        lifecycle.scheduleTestCase(result);
        lifecycle.startTestCase(uuid);
    }


    public void testFinished(final TestInfo testInfo) {
        String uuid = testInfo.getTestId();

        lifecycle.updateTestCase(uuid, testResult -> {
            if (Objects.isNull(testResult.getStatus())) {
                testResult.setStatus(Status.PASSED);
            }
        });

        lifecycle.stopTestCase(uuid);
        lifecycle.writeTestCase(uuid);
    }


//    public void testFailure(final Failure failure) {
//        final String uuid = testCases.get();
//        lifecycle.updateTestCase(uuid, testResult -> testResult
//                .setStatus(getStatus(failure.getException()).orElse(null))
//                .setStatusDetails(getStatusDetails(failure.getException()).orElse(null))
//        );
//    }


//    @Override
//    public void testIgnored(final Description description) {
//        final String uuid = testCases.get();
//        testCases.remove();
//
//        final TestResult result = createTestResult(uuid, description);
//        result.setStatus(Status.SKIPPED);
//        result.setStatusDetails(getIgnoredMessage(description));
//        result.setStart(System.currentTimeMillis());
//
//        getLifecycle().scheduleTestCase(result);
//        getLifecycle().stopTestCase(uuid);
//        getLifecycle().writeTestCase(uuid);
//    }


//    private List<Link> extractLinks(final Description description) {
//        final List<Link> result = new ArrayList<>(getLinks(description.getAnnotations()));
//        Optional.of(description)
//                .map(Description::getTestClass)
//                .map(AnnotationUtils::getLinks)
//                .ifPresent(result::addAll);
//        return result;
//    }
//
//    private List<Label> extractLabels(final Description description) {
//        final List<Label> result = new ArrayList<>(getLabels(description.getAnnotations()));
//        Optional.of(description)
//                .map(Description::getTestClass)
//                .map(AnnotationUtils::getLabels)
//                .ifPresent(result::addAll);
//        return result;
//    }

    private String getHistoryId(TestInfo testInfo) {
        return md5(testInfo.getFullTestName());
    }

    private String getPackage(final Class<?> testClass) {
        return Optional.ofNullable(testClass)
                .map(Class::getPackage)
                .map(Package::getName)
                .orElse("");
    }

//    private StatusDetails getIgnoredMessage(final Description description) {
//        final Ignore ignore = description.getAnnotation(Ignore.class);
//        final String message = Objects.nonNull(ignore) && !ignore.value().isEmpty()
//                ? ignore.value() : "Test ignored (without reason)!";
//        return new StatusDetails().setMessage(message);
//    }


    private io.qameta.allure.model.TestResult createTestResult(final String uuid, TestInfo testInfo) {
        final String className = testInfo.getTestClass().getCanonicalName();
        final String methodName = testInfo.getTestMethodName();
        final String name = Objects.nonNull(methodName) ? methodName : className;
        final String fullName = Objects.nonNull(methodName) ? String.format("%s.%s", className, methodName) : className;
//        final String suite = Optional.ofNullable(description.getTestClass())
//                .map(it -> it.getAnnotation(DisplayName.class))
//                .map(DisplayName::value).orElse(className);

        io.qameta.allure.model.TestResult testResult = new io.qameta.allure.model.TestResult();
        testResult.setUuid(uuid)
                .setHistoryId(getHistoryId(testInfo))
                .setFullName(fullName)
                .setName(name);

//        testResult.getLabels().addAll(getProvidedLabels());
//        testResult.getLabels().addAll(Arrays.asList(
//                createPackageLabel(getPackage(description.getTestClass())),
//                createTestClassLabel(className),
//                createTestMethodLabel(name),
//                createSuiteLabel(suite),
//                createHostLabel(),
//                createThreadLabel(),
//                createFrameworkLabel("junit4"),
//                createLanguageLabel("java")
//        ));
//        testResult.getLabels().addAll(extractLabels(description));
//        testResult.getLinks().addAll(extractLinks(description));

        //dell
//        getDisplayName(description).ifPresent(testResult::setName);
//        getDescription(description).ifPresent(testResult::setDescription);
        return testResult;
    }

    @EventListener
    @Async //todo validate async>
    public void testStart(TestStartEvent testStartEvent) {
        TestInfo testInfo = testStartEvent.getTestInfo();


        Class testClass = testInfo.getTestClass();
//        final String className = testInfo.getTestClass().getCanonicalName();
//        final String methodName = testInfo.getTestMethodName();
//        final String name = Objects.nonNull(methodName) ? methodName : className;
//        final String fullName = Objects.nonNull(methodName) ? String.format("%s.%s", className, methodName) : className;
//        final String suite = Optional.ofNullable(description.getTestClass())
//                .map(it -> it.getAnnotation(DisplayName.class))
//                .map(DisplayName::value).orElse(className);

        TestResult testResult = new TestResult();

        String name = null, description = null;
        if (testInfo.getYafTest() != null) {
            YafTest yafTest = testInfo.getYafTest();
            name = yafTest.name();
            description = yafTest.description();
            //testResult.setTestCaseId()
        }
        if (Strings.EMPTY.equals(name)) {
            name = testInfo.getTestName();
        }

        testResult.setUuid(testInfo.getTestId())
                .setHistoryId(getHistoryId(testInfo))
                .setFullName(testInfo.getFullTestName())
                .setName(name)
                .setDescription(description);

        List<Label> labels = testResult.getLabels();
        List<Link> links = testResult.getLinks();

        links.addAll(Arrays.asList(
                createTmsLink("zzz-12")
        ));

        labels.addAll(Arrays.asList(
                createPackageLabel(getPackage(testClass)),
                createTestClassLabel(testClass.getCanonicalName()),
                createTestMethodLabel(name),
                createSuiteLabel(testInfo.getSuiteInfo().getSuiteName()),
                createHostLabel(),
                createThreadLabel(),
                createFrameworkLabel("junit4"),
                createLanguageLabel("java")
        ));

        //    private List<Link> extractLinks(final Description description) {
//        final List<Link> result = new ArrayList<>(getLinks(description.getAnnotations()));
//        Optional.of(description)
//                .map(Description::getTestClass)
//                .map(AnnotationUtils::getLinks)
//                .ifPresent(result::addAll);
//        return result;
//    }
//
//    private List<Label> extractLabels(final Description description) {
//        final List<Label> result = new ArrayList<>(getLabels(description.getAnnotations()));
//        Optional.of(description)
//                .map(Description::getTestClass)
//                .map(AnnotationUtils::getLabels)
//                .ifPresent(result::addAll);
//        return result;
//    }

        //add method params
        if (testInfo.getTestMethodParams() != null) {
            List<Parameter> parameterList = testInfo.getTestMethodParams().entrySet().stream().map(e ->
                    new Parameter().setName(e.getKey()).setValue(e.getValue().toString())
            ).collect(Collectors.toList());
            testResult.setParameters(parameterList);
        }


//        testResult.getLabels().addAll(getProvidedLabels());
//        testResult.getLabels().addAll(Arrays.asList(
//                createPackageLabel(getPackage(description.getTestClass())),
//                createTestClassLabel(className),
//                createTestMethodLabel(name),
//                createSuiteLabel(suite),
//                createHostLabel(),
//                createThreadLabel(),
//                createFrameworkLabel("junit4"),
//                createLanguageLabel("java")
//        ));
//        testResult.getLabels().addAll(extractLabels(description));
//        testResult.getLinks().addAll(extractLinks(description));


        lifecycle.scheduleTestCase(testResult);
        lifecycle.startTestCase(testInfo.getTestId());

//        testInfo.getTestParams().put(UID, uuid);
//        io.qameta.allure.model.TestResult result = createTestResult(uuid, testInfo);
//        lifecycle.scheduleTestCase(result);
//        lifecycle.startTestCase(uuid);
//
//
//        testStarted(testStartEvent.getTestInfo());
//        String uid = testStartEvent.getTestInfo().getFullTestName();
//        TestResultContainer testResultContainer = new TestResultContainer();
//        testResultContainer.setUuid("aaa").setDescription("container").setStart(System.currentTimeMillis());
//
//        lifecycle.startTestContainer("aaa", testResultContainer);
//        io.qameta.allure.model.TestResult tr = new io.qameta.allure.model.TestResult();
//        tr.setName("tr").setDescription("trtttttt").setFullName("fulleNAme").setUuid(uid);
//        lifecycle.scheduleTestCase("aaa", tr);
//        lifecycle.startTestCase(uid);

//        lifecycle.getCurrentTestCaseOrStep().ifPresent(parentUuid -> {
//            TestInfo testInfo = testStartEvent.getTestInfo();
//            final String uuid = UUID.randomUUID().toString();
//            //TODO build correct start result with desc and so on
//            lifecycle.startStep(parentUuid, uuid, new StepResult().setName(testInfo.getTestName()));
//
//        });
    }

    @EventListener
    //@Async //todo validate async>
    public void testFinish(TestFinishEvent testFinishEvent) {
        testFinished(testFinishEvent.getTestInfo());
//        TestInfo testInfo = testFinishEvent.getTestInfo();
//        TestResult testResult = testFinishEvent.getTestResult();
//
//        String uid = testInfo.getFullTestName();
//
//        lifecycle.updateTestCase(uid, (testCase)->{
//            testCase.setDescription("SDASDASDA");
//        });
//
//        lifecycle.stopTestCase(uid);
//
//        if (!testResult.isSuccess() || allureProperties.isFullLogAllTests()) {
//            lifecycle.getCurrentTestCaseOrStep().ifPresent(parentUuid -> {
//                if (testFinishEvent.getScreenshots() != null) {
//                    testFinishEvent.getScreenshots().entrySet().forEach(e -> {
//                        lifecycle.addAttachment("screenshot_" + e.getKey(), "image/png", "png", e.getValue());
//                    });
//                }
//                if (testFinishEvent.getPageSource() != null) {
//                    lifecycle.addAttachment("Page source", "text/html", "html", testFinishEvent.getPageSource().getBytes(charset));
//                }
//                if (testFinishEvent.getLogs() != null) {
//                    testFinishEvent.getLogs().entrySet().forEach(e -> {
//                        lifecycle.addAttachment("Logs from: " + e.getKey(), "application/json", ".txt", e.getValue().getBytes(charset));
//                    });
//                }
//            });
//        }
//
//        lifecycle.getCurrentTestCaseOrStep().ifPresent(parentUuid -> {
//            switch (testResult.getState()) {
//                case SUCCESS:
//                    lifecycle.updateStep(step -> step.setStatus(Status.PASSED));
//                    break;
//                case FAIL:
//                    lifecycle.updateStep(stepResult -> {
//                        stepResult.setStatus(Status.BROKEN);
//                        stepResult.setStatusDetails(getStatusDetails(testResult.getError()).orElse(new StatusDetails()));
//                    });
//                    break;
//                case SKIP:
//                    lifecycle.updateStep(stepResult -> {
//                        stepResult.setStatus(Status.SKIPPED);
//                        stepResult.setStatusDetails(new StatusDetails().setMessage("Test skipped!"));
//                    });
//                    break;
//                default:
//                    log.warn("Test finished with unknown state!");
//                    //TODO
//                    break;
//            }
//            lifecycle.stopStep();
//        });
    }
}
