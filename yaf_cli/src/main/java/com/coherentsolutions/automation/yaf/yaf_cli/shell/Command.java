package com.coherentsolutions.automation.yaf.yaf_cli.shell;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;

@Data
@Accessors(chain = true)
public class Command {
    boolean remote;
    List<String> args = new ArrayList<String>();
    Map<String, String> env = new HashMap<String, String>();
    Long timeOut = 60000l; //1 minute
    String workDir;

    public static Command build(String command, boolean remote) throws IllegalArgumentException {
        Command c = build(command);
        c.setRemote(remote);
        return c;
    }

    public static Command build(String command) throws IllegalArgumentException {
        try {
            String[] splitted = command.split("\\s");
            Command c = new Command();
            List<String> args = Arrays.asList(splitted);
            c.setArgs(args);
            return c;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Unable to build command from string " + command);
        }
    }
}
