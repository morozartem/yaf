package com.coherentsolutions.automation.yaf.yaf_cli.shell;

import com.coherentsolutions.automation.yaf.yaf_cli.shell.exec.LocalExec;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.exec.RemoteExec;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ShellService {


    @Autowired
    RemoteExec remoteExec;

    @Autowired
    LocalExec localExec;


    public ShellResult exec(String command) {
        return exec(command, false);
    }

    public ShellResult exec(String command, boolean remote) {
        return exec(Command.build(command, remote));
    }

    public ShellResult exec(Command command) {

        log.info("--Start execute command");
        ShellResult result;
        if (command.isRemote()) {
            result = remoteExec.exec(command);
        } else {
            result = localExec.exec(command);
        }
        log.info("--Got result :: " + result);

        return result;
    }

}
