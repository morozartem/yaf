package com.coherentsolutions.automation.yaf.yaf_cli.shell.exec;

import com.coherentsolutions.automation.yaf.yaf_cli.ShellProperties;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.Command;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.ShellResult;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.coherentsolutions.automation.yaf.yaf_cli.shell.Consts.APIKEY_HEADER;
import static io.restassured.RestAssured.given;
import static io.restassured.config.HttpClientConfig.httpClientConfig;

@Service
public class RemoteExec implements Exec {

    @Autowired
    ShellProperties properties;

    RequestSpecification specification;

    public RemoteExec() {
        long timeOut = properties.getRemote().getTimeOut();
        ShellProperties.RemoteShellProperties remote = properties.getRemote();
        RestAssuredConfig config = RestAssuredConfig.config()
                .httpClient(httpClientConfig()
                        .setParam("http.connection-manager.timeout", timeOut)
                        .setParam("http.connection.timeout", timeOut)
                        .setParam("http.socket.timeout", timeOut));
        specification = given()
                .config(config)
                .header(APIKEY_HEADER, remote.getApiKey())
                .contentType(ContentType.JSON).baseUri(remote + "://" + remote.getRemoteHost() + ":" + remote.getRemotePort() + "/exec");
    }

    @Override
    public ShellResult exec(Command command) {
        ShellResult result = specification
                .body(command)
                .post().as(ShellResult.class);
        return result;
    }
}
