package com.coherentsolutions.automation.yaf.yaf_cli.shell;

public class Consts {

    public static final String $USER_DIR = "$userDir";
    public static final String $FS = "$fs";

    public static final String TEMP_FOLDER = "$userDir" + $FS + "tmp" + $FS;
    public static final String TEMP_CONFIG_FOLDER = TEMP_FOLDER + "config" + $FS;
    public static final String TEMP_INSTALL_FOLDER = TEMP_FOLDER + "install" + $FS;


    public static final String APIKEY_HEADER = "X-RemoteShellKey";


}
