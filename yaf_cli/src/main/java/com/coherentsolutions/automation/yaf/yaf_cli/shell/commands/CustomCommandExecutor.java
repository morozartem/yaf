package com.coherentsolutions.automation.yaf.yaf_cli.shell.commands;


import com.coherentsolutions.automation.yaf.yaf_cli.shell.Command;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.ShellResult;
import org.springframework.stereotype.Component;

@Component
public interface CustomCommandExecutor {

    String getCommandName();

    ShellResult execute(Command command);
}
