package com.coherentsolutions.automation.yaf.yaf_cli.shell.exec;

import com.coherentsolutions.automation.yaf.yaf_cli.shell.Command;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.ShellResult;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.commands.CustomCommandExecutor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SystemUtils;
import org.buildobjects.process.ExternalProcessFailureException;
import org.buildobjects.process.ProcBuilder;
import org.buildobjects.process.ProcResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.coherentsolutions.automation.yaf.yaf_cli.shell.Consts.$FS;
import static com.coherentsolutions.automation.yaf.yaf_cli.shell.Consts.$USER_DIR;

@Service
@Slf4j
public class LocalExec implements Exec {

    String userDir = System.getProperty("user.dir");

    boolean isWindows = SystemUtils.OS_NAME.toLowerCase().contains("windows");

    List<String> macOsCommands = Arrays.asList("ping", "adb");//todo add configurability


    @Autowired
    private List<CustomCommandExecutor> executors;

    @Override
    public ShellResult exec(Command command) {
        ShellResult result;
        List<String> args = command.getArgs().stream().map(a -> processArg(a)).collect(Collectors.toList());
        command.setArgs(args);
        log.info("--Args after processing :: " + command.getArgs());
        String c = args.get(0);

        CustomCommandExecutor executor = executors.stream().filter(e -> e.getCommandName().equalsIgnoreCase(c)).findFirst().orElse(null);
        if (executor != null) {
            log.info("--User custom executor :: " + executor.getClass().getSimpleName());
            result = executor.execute(command);
        } else {

            ProcBuilder pb;
            if (isWindows) {
                pb = new ProcBuilder("cmd").withArg("/q").withArgs("/c");
            } else {
                if (!macOsCommands.contains(c)) {
                    pb = new ProcBuilder("bash")
                            .withArg("/l")
                            .withArgs("/c");
                } else {
                    pb = new ProcBuilder(c);
                    command.getArgs().remove(0);
                }
            }
            command.getArgs().forEach(a -> pb.withArgs(a));
            command.getEnv().entrySet().forEach(e -> pb.withVar(e.getKey(), e.getValue()));
            if (command.getWorkDir() != null) {
                pb.withWorkingDirectory(new File(processArg(command.getWorkDir())));
            }

            pb.withTimeoutMillis(command.getTimeOut());
            result = new ShellResult();
            try {
                ProcResult res = pb.run();
                result.setOutput(res.getOutputString());
                result.setErrorOutput(res.getErrorString());
                result.setExitCode(res.getExitValue());
            } catch (ExternalProcessFailureException ex) {
                if (3010 != ex.getExitValue()) {
                    //lets skip this exit code
                    throw ex;
                } else {
                    result.setOutput("Default restart needed exit code!");
                    result.setExitCode(0);
                }
            }
        }
        return result;
    }

    public String processArg(String arg) {
        if (arg.contains("$")) {
            arg = arg.replace($USER_DIR, userDir);
            arg = arg.replace($FS, File.separator);
            //some other vars
        }
        return arg;
    }
}
