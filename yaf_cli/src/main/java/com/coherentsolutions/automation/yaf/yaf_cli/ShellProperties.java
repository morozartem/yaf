package com.coherentsolutions.automation.yaf.yaf_cli;

import com.coherentsolutions.automation.yaf.config.yaf.ModuleProperties;
import com.coherentsolutions.automation.yaf.consts.Consts;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = Consts.FRAMEWORK_NAME + ".shell")
public class ShellProperties extends ModuleProperties {

    List<String> unixCommands = Arrays.asList("ping", "adb");
    RemoteShellProperties remote;

    @Data
    @Configuration
    @ConfigurationProperties(prefix = Consts.FRAMEWORK_NAME + ".shell.remote")
    public class RemoteShellProperties {

        long timeOut = 180000;
        String apiKey = "0000-0000-1111-1111";
        String remoteHost;
        int remotePort = 9876;
        String protocol = "http";

    }
}
