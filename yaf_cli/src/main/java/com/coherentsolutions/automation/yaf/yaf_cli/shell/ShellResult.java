package com.coherentsolutions.automation.yaf.yaf_cli.shell;

import lombok.Data;

@Data
public class ShellResult {

    String output;
    String errorOutput;
    int exitCode;

    public static ShellResult build(Boolean output) {
        return ShellResult.build(String.valueOf(output));
    }

    public static ShellResult build(String output) {
        ShellResult res = new ShellResult();
        res.setOutput(output);
        res.setExitCode(0);
        return res;
    }

    public static ShellResult error(String error) {
        ShellResult res = new ShellResult();
        res.setErrorOutput(error);
        res.setExitCode(1);
        return res;
    }

    public boolean equals(boolean bool) {
        return output.equals(String.valueOf(bool));
    }

    public boolean valid() {
        return exitCode == 0;
    }

    public void normalizeOutput() {
        if (output != null)
            output = normalizeString(output);
        if (errorOutput != null)
            errorOutput = normalizeString(errorOutput);
    }

    private String normalizeString(String string) {
        return string.replaceAll("\n", " ").replaceAll("\t", " ").replaceAll("\\s+", " ");
    }
}
