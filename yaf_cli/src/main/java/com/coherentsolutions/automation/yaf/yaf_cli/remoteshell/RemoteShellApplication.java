package com.coherentsolutions.automation.yaf.yaf_cli.remoteshell;


import com.coherentsolutions.automation.yaf.yaf_cli.ShellProperties;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.Command;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.Consts;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.ShellResult;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.ShellService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@Slf4j
public class RemoteShellApplication {

    @Autowired
    ShellService service;
    @Autowired
    ShellProperties properties;

    public static void main(String[] args) {
        SpringApplication.run(RemoteShellApplication.class, args);
    }

    @RestController
    @RequestMapping("/")
    public class RemoteController {

        @PostMapping(value = "exec", consumes = MediaType.APPLICATION_JSON_VALUE)
        public ShellResult execute(@RequestBody Command command) {
            log.info("EXECUTE:: " + command);
            command.setRemote(false);
            return service.exec(command);
        }

        @GetMapping(value = "state")
        public boolean state() {
            return true;
        }
    }

    @Configuration
    @EnableWebSecurity
    @Order(1)
    public class APISecurityConfig extends WebSecurityConfigurerAdapter {


        @Override
        protected void configure(HttpSecurity httpSecurity) throws Exception {
            APIKeyAuthFilter filter = new APIKeyAuthFilter();
            filter.setAuthenticationManager(authentication -> {
                String principal = (String) authentication.getPrincipal();
                if (!properties.getRemote().getApiKey().equals(principal)) {
                    throw new BadCredentialsException("The API key was not found or not the expected value.");
                }
                authentication.setAuthenticated(true);
                return authentication;
            });
            httpSecurity.
                    antMatcher("/state").anonymous().and().
                    antMatcher("/exec").
                    csrf().disable().
                    sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                    and().addFilter(filter).authorizeRequests().anyRequest().authenticated();
        }

    }

    public class APIKeyAuthFilter extends AbstractPreAuthenticatedProcessingFilter {

        @Override
        protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
            return request.getHeader(Consts.APIKEY_HEADER);
        }

        @Override
        protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
            return "N/A";
        }

    }

}
