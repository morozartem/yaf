package com.coherentsolutions.automation.yaf.yaf_cli.shell.commands.windows;


import com.coherentsolutions.automation.yaf.yaf_cli.shell.Command;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.ShellResult;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.commands.CustomCommandExecutor;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class WindowsCreateDirExecutor implements CustomCommandExecutor {

    public static final String NAME = "wDirCreate";

    @Override
    public String getCommandName() {
        return NAME;
    }

    @Override
    public ShellResult execute(Command command) {
        String path = command.getArgs().get(1);
        try {
            Files.createDirectories(Paths.get(path));
            return ShellResult.build(true);
        } catch (Exception ex) {
            return ShellResult.error(ex.getMessage());
        }
    }
}
