package com.coherentsolutions.automation.yaf.yaf_cli.shell.exec;

import com.coherentsolutions.automation.yaf.yaf_cli.shell.Command;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.ShellResult;
import org.springframework.stereotype.Service;

@Service
public interface Exec {

    ShellResult exec(Command command);
}
