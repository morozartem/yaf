package com.coherentsolutions.automation.yaf.yaf_cli.shell.commands.windows;

import com.coherentsolutions.automation.yaf.yaf_cli.shell.Command;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.ShellResult;
import com.coherentsolutions.automation.yaf.yaf_cli.shell.commands.CustomCommandExecutor;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class WindowsFileExistsExecutor implements CustomCommandExecutor {

    public static final String NAME = "wFileExists";

    @Override
    public String getCommandName() {
        return NAME;
    }

    @Override
    public ShellResult execute(Command command) {
        String path = command.getArgs().get(1);
        return ShellResult.build(new File(path).exists());
    }
}
