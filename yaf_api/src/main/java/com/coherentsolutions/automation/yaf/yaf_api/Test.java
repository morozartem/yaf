package com.coherentsolutions.automation.yaf.yaf_api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.specification.RequestSpecification;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Test {


    public static void main(String[] args) {
        RestAssuredConfig config = new RestAssuredConfig();
        LogConfig logConfig = new LogConfig();

        config.logConfig(logConfig);
        RequestSpecification specification = new RequestSpecBuilder()
                .build().config(config);

    }
}
