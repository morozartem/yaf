package com.coherentsolutions.automation.yaf.web.pom;

import com.coherentsolutions.automation.yaf.bean.pom.Page;
import com.coherentsolutions.automation.yaf.bean.pom.factory.YafLocatorFactory;
import com.coherentsolutions.automation.yaf.drivers.properties.DriverProperties;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class WebPage extends Page {

    @Autowired
    DriverProperties driverProperties;

    @Autowired
    YafLocatorFactory locatorFactory;

    @Override
    protected void init() {
        //todo think about getting names from annotations or someWhereElse
        driverHolder = testExecutionContext.getWebDriverHolder();

        if (driverProperties.getWeb() != null && driverProperties.getWeb().isUsePageFactory()) {
            locatorFactory.setSearchContext(driverHolder.getDriver());
            PageFactory.initElements(locatorFactory, this);
        }
    }
}
