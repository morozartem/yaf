package com.coherentsolutions.automation.yaf.web.driver;

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import org.springframework.stereotype.Service;

import static com.coherentsolutions.automation.yaf.consts.Consts.FARM_BROWSERSTACK;

@Service
public class BrowserStackWebDriverResolver extends AbstractWebDriverResolver {

    @Override
    public boolean canResolve(EnvItem envItem) {
        return super.canResolve(envItem) && farmName(envItem, FARM_BROWSERSTACK);
    }

    @Override
    public DriverHolder initDriver(EnvItem envItem) {
        System.out.println("------ BUILD BS DRIVER --------- ");
        DriverHolder driverHolder = new DriverHolder(envItem);
        driverHolder.setDeviceType(DeviceType.WEB);
        return driverHolder;
    }
}
