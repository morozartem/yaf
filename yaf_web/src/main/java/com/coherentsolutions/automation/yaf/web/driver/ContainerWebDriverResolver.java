package com.coherentsolutions.automation.yaf.web.driver;

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import com.coherentsolutions.automation.yaf.web.driver.holder.ContainerDriverHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.testcontainers.containers.BrowserWebDriverContainer;

@Component
@Slf4j
public class ContainerWebDriverResolver extends AbstractWebDriverResolver{

    @Override
    public boolean canResolve(EnvItem envItem) {
        return super.canResolve(envItem) && farmName(envItem, "container");//TODO!!!
    }

    @Override
    public DriverHolder initDriver(EnvItem envItem) {
        BrowserWebDriverContainer<?> c = new BrowserWebDriverContainer<>()
                .withCapabilities(buildCapabilitiesFromEnv(envItem));

        c.start();
        log.info("Selenium URL {}", c.getSeleniumAddress());
        log.info("VNC URL {}", c.getVncAddress());
        ContainerDriverHolder driverHolder = new ContainerDriverHolder(envItem);
        driverHolder.setDriver(c.getWebDriver());
        driverHolder.setDeviceType(DeviceType.WEB);
        driverHolder.setContainer(c);
        return driverHolder;
    }
}
