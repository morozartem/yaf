package com.coherentsolutions.automation.yaf.web.condition;

import com.coherentsolutions.automation.yaf.condition.YafCondition;
import com.coherentsolutions.automation.yaf.enums.Browser;
import com.coherentsolutions.automation.yaf.enums.DeviceType;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
@Documented
@YafCondition(browser = Browser.CHROME, deviceType = DeviceType.WEB)
public @interface Chrome {
}
