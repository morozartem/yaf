package com.coherentsolutions.automation.yaf.web.driver;

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.drivers.manager.DriverResolver;
import com.coherentsolutions.automation.yaf.enums.DeviceType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractWebDriverResolver extends DriverResolver {


    @Override
    public DeviceType getResolverType() {
        return DeviceType.WEB;
    }


    protected ChromeDriver buildChromeDriver(EnvItem envItem) {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.merge(buildCapabilitiesFromEnv(envItem));

        chromeOptions.setCapability("resolution", "1920x1080");
        chromeOptions.setHeadless(envItem.isHeadless());
        return new ChromeDriver(chromeOptions);
    }

    protected FirefoxDriver buildFireFoxDriver(EnvItem envItem) {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.merge(buildCapabilitiesFromEnv(envItem));
        firefoxOptions.setHeadless(envItem.isHeadless());
        return new FirefoxDriver(firefoxOptions);
    }

    //TODO add others

}
