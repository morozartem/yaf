package com.coherentsolutions.automation.yaf.web.driver.holder;

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import lombok.Getter;
import lombok.Setter;
import org.testcontainers.containers.BrowserWebDriverContainer;


public class ContainerDriverHolder extends DriverHolder {

    @Getter
    @Setter
    BrowserWebDriverContainer container;

    public ContainerDriverHolder(EnvItem envItem) {
        super(envItem);
    }

    @Override
    public void quit() {
        super.quit();
        container.stop();
        //TODO exceptions?
    }
}
