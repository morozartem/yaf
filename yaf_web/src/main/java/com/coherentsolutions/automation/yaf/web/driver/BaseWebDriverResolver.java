package com.coherentsolutions.automation.yaf.web.driver;

import com.coherentsolutions.automation.yaf.config.env.EnvItem;
import com.coherentsolutions.automation.yaf.consts.Consts;
import com.coherentsolutions.automation.yaf.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.exception.DriverYafException;
import com.coherentsolutions.automation.yaf.utils.CapabilitiesUtils;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order()//set this resolver at last position, cause other user defined should be prior that
@Slf4j
public class BaseWebDriverResolver extends AbstractWebDriverResolver {

    @Override
    public boolean canResolve(EnvItem envItem) {
        return super.canResolve(envItem) && emptyFarm(envItem);
    }

    @Override
    public DriverHolder initDriver(EnvItem envItem) {

        WebDriver driver = null;
        log.debug("Building web {} driver for env {}", envItem.getBrowser(), envItem.getName());
        switch (envItem.getBrowser()) {
            case CHROME: {
                WebDriverManager.chromedriver().setup();
                driver = buildChromeDriver(envItem);
                break;
            }
            case FF: {
                WebDriverManager.firefoxdriver().setup();
                driver = buildFireFoxDriver(envItem);
                break;
            }
            //todo add others
            default: {
                throw new DriverYafException("SSSSSS");
            }
        }
//TODO match this with condition matcher and other cloud farms
        String resolution = envItem.getCapabilities().get(Consts.CAP_RESOLUTION);
        if (resolution != null) {
            driver.manage().window().setSize(CapabilitiesUtils.getDimensionFromResolution(resolution));
        }

        log.debug("****** CREATE NEW WEB DRIVER ******** ");
        return buildHolder(envItem, driver);
    }


}
